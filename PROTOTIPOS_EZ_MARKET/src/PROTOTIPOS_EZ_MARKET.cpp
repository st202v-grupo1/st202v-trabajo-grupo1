//============================================================================
// Name        : PROTOTIPOS_EZ_MARKET.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

/**********PROTOTIPOS PARA BODEGUERO**********/

void ModificarDatosBodega();
void mostrarVenta(string tipo,string valor);
void ConsultaVenta();
void ConsultaProductos();
void ConsultaRegistro();

void cargarCampo(string campo,string valor);
void modificarCampo(string campo);
void mostrarProductosBodega();
void ActualizarProducto();

void CargarOperacion(string tipoOp,string codigo,string fecha,int i);
void CargarDatosProducto(string codigo,string tipo,string nombre, string unidad,string stock,string stockS,string precioCompra,string precioVenta);
bool ValidarCodigo(string n);
void MostrarListaProductos();
void RegistrarNuevoProducto();
void menuBodeguero();
void registrarBodeguero();

/*****PROTOTIPOS PARA CLIENTE*******/
void ModificarDatosCliente();

bool validaTarjeta(string num);
void MododePago(string modo);
void FinalizarCompra();

void mostrarReporteCompra(string codigo,int cantidad,int i);
void mostrarProductos(string tipo);
void mostrarTiposDeProducto();
void SolicitudProducto();
void mostrarFecha(string fecha);
void mostrarPro(string producto);
void mostrarCompras(string filtro);
void consultadeOperaciones();

void opcionesCliente(int opcion);
void registrarCliente();
void menuCliente();
void opcionUno();
void opcionDos();
void opcionTres();
void MenuDeEntrada();



/***********PRINCIPAL**************/
int main() {
	MenuDeEntrada();

	return 0;
}



/********************IMPLEMENTACION DE FUNCIONES********************/

void MenuDeEntrada(){
	cout<<"Bienvenido a EZ Market"<<endl;
	cout<<"(1)Ingresar"<<endl;
	cout<<"(2)Registrar"<<endl;
	cout<<"(3)Salir"<<endl;
	cout <<"Escoger la opcion: ";
	int m;
	cin>>m;
	switch(m){
	case 1:
		opcionUno();
		break;
	case 2:
		opcionDos();
		break;
	case 3:
		opcionTres();
		break;
	}
}

void opcionUno(){
	cout<<"Bienvenido a EZ Market"<<endl;
	cout<<"Usuario: daviAg"<<endl;
	cout<<"Contrasenia: ***********"<<endl;
	string rol="cliente";

	if(rol=="cliente"){
		menuCliente();
	}else{
		menuBodeguero();
	}
}

void opcionDos(){
	cout<<"Registro de Usuario"<<endl;
	cout<<"Rol: "<<endl;
	string rol;
	cin>>rol;
	cout<<endl;
	if(rol=="cliente"){
		registrarCliente();
	}
	if(rol=="bodeguero"){
		registrarBodeguero();
	}
}

/*******CLIENTE**********/
void registrarCliente(){
	cout<<"Nombre de usuario: DavidAg"<<endl;
	cout<<"Nombre de Cliente: david"<<endl;
	cout<<"Apellido de Cliente: agapito"<<endl;
	cout<<"Sexo: M"<<endl;
	cout<<"Correo: davidaq@gmail.com"<<endl;
	cout<<"Direccion: rimac"<<endl;
	cout<<"Telefono: 933454538"<<endl;
	cout<<"Contrase�a: *****"<<endl;
	cout<<"\t\tCLIENTE REGISTRADO"<<endl;
	menuCliente();

}

void menuCliente(){
	cout<<"OPCIONES"<<endl;
		cout<<"1. Consulta De Operaciones"<<endl;
		cout<<"2. Solicitud Producto"<<endl;
		cout<<"3. Finalizar Compra"<<endl;
		cout<<"4. Modificar Datos"<<endl;
		cout<<"5. Regresar"<<endl;
		int m;
		cin>>m;
		opcionesCliente(m);
}

void opcionesCliente(int opcion){
	switch(opcion){
	case 1: consultadeOperaciones();
		break;
	case 2: SolicitudProducto();
		break;
	case 3: FinalizarCompra();
		break;
	case 4: ModificarDatosCliente();
		break;
	case 5: MenuDeEntrada();
		break;
	default: cout<<"Opcion no valida"<<endl;
	}
}


void consultadeOperaciones(){
	cout<<"1. Mostrar Compras"<<endl;
	cout<<"2. Filtro"<<endl;
	cout<<"3. Regresar"<<endl;
	int opcion;
	cout<<"Ingrese Opcion";	cin>>opcion;
	string filtro;
	if(opcion==1){
		filtro="nada";
		mostrarCompras(filtro);
	}
	if(opcion==2){
		cout<<"(1)Producto"<<endl;
		cout<<"(2)Fecha"<<endl;
		int opcionFiltro;
		cin>>opcionFiltro;
		if(opcionFiltro==1){
			filtro="producto";
			mostrarCompras(filtro);
		}
		if(opcionFiltro==2){
			filtro="fecha";
			mostrarCompras(filtro);
				}

	}
	if(opcion==3){
		menuCliente();
	}
}

void mostrarCompras(string filtro){
	string resp;
	if(filtro=="nada"){
	 cout<<"cod 	tipo	   nombre	 capacidad"<<endl;
 	 cout<<"100 	bebida	   cielo	 500ml"<<endl;
	 cout<<"123 	bebida	   crush	 500ml"<<endl;
	 cout<<"254	    dulces	   morocha	 500gr"<<endl;
	 cout<<"352     menestras  lenteja   1 kg "<<endl;
	 cout<<"Regresar? ==> ";cin>>resp;
	 if(resp=="s"){
		consultadeOperaciones();
	 }
	}
	if(filtro=="producto"){
		string producto;
		cout<<"Ingrese nombre del producto: ";cin>>producto;
		mostrarPro(producto);
		cout<<"Regresar? ==> ";cin>>resp;
			 if(resp=="s"){
				consultadeOperaciones();
			 }
	}
	if(filtro=="fecha"){
		string fecha;
		cout<<"Ingrese fecha: ";cin>>fecha;
		mostrarFecha(fecha);
		cout<<"Regresar? ==> ";cin>>resp;
					 if(resp=="s"){
						consultadeOperaciones();
					 }

	}
}

void mostrarPro(string producto){
	cout<<"254	dulces	morocha	500ml"<<endl;
}
void mostrarFecha(string fecha){
	 cout<<"cod 	tipo	nombre	capacidad"<<endl;
	 cout<<"100 	bebida	cielo	500ml"<<endl;
	 cout<<"123 	bebida	crush	500ml"<<endl;
}

void SolicitudProducto(){
	cout<<"Buscar por: "<<endl;
	cout<<"(1)Tipo de Producto"<<endl;
	cout<<"(2)Nombre del Producto"<<endl;
	int opcionProducto;
	string resp;
	cout<<"Escoja opcion ==> ";	cin>>opcionProducto;
	if(opcionProducto==1){
		int r=0,i=1;
		while (r==0){
		mostrarTiposDeProducto();
		string tipo;
		cout<<"Elija el tipo de producto ==> ";cin>>tipo;
		mostrarProductos(tipo);
		cout<<"Agregar Carrito? ==> ";cin>>resp;
		if(resp=="s"){
		 string codigoProducto;
		 int cantidad;
		 cout<<"Codigo del producto ==> ";cin>>codigoProducto;
		 cout<<"Cantidad ==> ";cin>>cantidad;
		 mostrarReporteCompra(codigoProducto,cantidad,i);

		string opcion;
		cout<<"Elegir otro producto? ==> ";cin>>opcion;
		if(opcion=="s"){
			i++;
			r=0;
		}
		if(opcion=="n"){
			menuCliente();
		}
		}
		}
	}
	if(opcionProducto==2){
		int k=0,j=1;
		while (k==0){

				 string nombreProducto;
				 int cantidad;
				 string codigoProducto="100";
				 cout<<"Nombre del producto ==> ";cin>>nombreProducto;
				 cout<<"Cantidad ==> ";cin>>cantidad;
				 mostrarProductos(nombreProducto);
				 cout<<"Agregar Carrito? ==> ";cin>>resp;
				 if(resp=="s"){
				 int cant;
				 cout<<"Cantidad ==> ";cin>>cant;
				 mostrarReporteCompra(codigoProducto,cant,j);
				 string opcion;
				 cout<<"Elegir otro producto? ==> ";cin>>opcion;
				 if(opcion=="s"){
				     	j++;
			 	    	k=0;
				 }
				if(opcion=="n"){
					menuCliente();
				}
				}
				}

	}
}

void mostrarTiposDeProducto(){

	cout<<" Bebidas "<<endl;
	cout<<" Dulces "<<endl;
	cout<<" Menestras "<<endl;

}

void mostrarProductos(string tipo){
	if(tipo=="Bebidas"){

		cout<<"cod 	tipo	   nombre	 capacidad  precio"<<endl;
		cout<<"100 	bebida	   cielo	 500ml      2  "<<endl;
		cout<<"123 	bebida	   crush	 500ml      2.5 "<<endl;
	}
	if(tipo=="Dulces"){
		cout<<"cod 	tipo	   nombre	 capacidad  precio"<<endl;
		cout<<"254	dulces	   morocha	 500gr      1 "<<endl;
	}
	if(tipo=="Menestras"){
		cout<<"cod 	tipo	   nombre	 capacidad  precio"<<endl;
		cout<<"352  menestras  lenteja   1 kg       3 "<<endl;
	}
	if(tipo=="cielo"){
		cout<<"cod 	tipo	   nombre	 capacidad  precio"<<endl;
		cout<<"100 	bebida	   cielo	 500ml      2  "<<endl;
	}

}

void mostrarReporteCompra(string codigo,int cantidad,int i){
    if(i==1){
	cout<<"cod 	   tipo	   nombre    capacidad  precio  cantidad  Monto"<<endl;
	cout<<codigo<<"     bebida  cielo     500 ml     2         "<<cantidad<<"         4 "<<endl;
    }
    if(i==2){
   	cout<<"cod 	   tipo	   nombre    capacidad  precio  cantidad  Monto"<<endl;
   	cout<<"100     bebida  cielo     500 ml     2       2         4 "<<endl;
   	cout<<codigo<<"     dulces  morocha   500 gr     1         "<<cantidad<<"         3 "<<endl;
    }
    if(i==3){
      	cout<<"cod 	   tipo	   nombre    capacidad  precio  cantidad  Monto"<<endl;
      	cout<<"100     bebida  cielo     500 ml     2       2         4 "<<endl;
      	cout<<"254     dulces  morocha   500 gr     1       3         3 "<<endl;
        cout<<"                                                       Total= 7  nuevos soles"<<endl;
    }
}
void FinalizarCompra(){
	string a,modo;
	string cod="254";
	int i=3,cant=3,op;
	mostrarReporteCompra(cod,cant,i);
	cout<<"Finalizar Compra? ==> ";cin>>a;

	if(a=="s"){
		cout<<"Ingresar ubicacion actual : Av.Tupac Amaru 210 "<<endl;
		cout<<"Bodegas disponibles"<<endl;
		cout<<"1. Donia Marta "<<endl;
		cout<<"2. Don Jorge"<<endl;
		cout<<"3. Don Pepe"<<endl;
		cout<<"Elija ==> ";cin>>op;
		if(op==3){
			cout<<"Ingrese modo de pago ==> ";cin>>modo;
			MododePago(modo);
		}
	}
	menuCliente();


}

void MododePago(string modo){
   string numTarjeta;
   if(modo=="tarjeta"){
	   cout<<"Ingrese numero de tarjeta = ";cin>>numTarjeta;
	   if(validaTarjeta(numTarjeta)){
		   cout<<"Tarjeta valida"<<endl;
		   string resp;
		   cout<<"Confirmar compra? ==> ";cin>>resp;
		   if(resp=="s"){
			   cout<<"Compra Realizada.   Enviando..."<<endl;
		   }

	   }else{
		   cout<<"NO EXISTE EL NUMERO DE TARJETA";menuCliente();
	   }
   }
}

bool validaTarjeta(string num){
	bool existe=true;
	int suma=10;
	if(suma%10==0){
		existe=true;
	}else{
		existe=false;
	}
	return existe;
}

void opcionTres(){
	cout<<endl;
	cout<<"*****GRACIAS POR SU VISITA A EZ MARKET :) *****";
}

void ModificarDatosCliente(){

}

/********BODEGUERO*********/

void registrarBodeguero(){
	cout<<"Nombre de usuario: edcc"<<endl;
	cout<<"Nombre de Bodeguero: ed"<<endl;
	cout<<"Apellido de Bodeguero: condori"<<endl;
	cout<<"Nombre de la Bodega: Don Pepe"<<endl;
	cout<<"RUC: 12434567563454"<<endl;
	cout<<"Sexo: M"<<endl;
	cout<<"Correo: edcc@gmail.com"<<endl;
	cout<<"Direccion: san martin de porres"<<endl;
	cout<<"Telefono: 932493923"<<endl;
	cout<<"ContraseÃ±a: ***************"<<endl;
	cout<<"\t\tBODEGA REGISTRADA"<<endl;
	menuBodeguero();
}
void menuBodeguero(){
	int a;
	cout<<"OPCIONES"<<endl;
	cout<<"1. Registrar nuevo producto"<<endl;
	cout<<"2. Actualizar"<<endl;
	cout<<"3. Consulta Registro Actualizacion"<<endl;
	cout<<"4. Consulta Ventas"<<endl;
	cout<<"5. Consulta Productos"<<endl;
	cout<<"6. Modificar Datos"<<endl;
	cout<<"7. Regresar"<<endl;
	cout<<"Escoja opcion ==> ";	cin>>a;
	switch(a){
	case 1: RegistrarNuevoProducto(); break;
	case 2: ActualizarProducto(); break;
	case 3: ConsultaRegistro(); break;
	case 4: ConsultaVenta(); break;
	case 5: ConsultaProductos(); break;
	case 6: ModificarDatosBodega(); break;
	case 7: MenuDeEntrada(); break;
	default: cout<<"Opcion no valida"<<endl;
	}
}
void RegistrarNuevoProducto(){
	string n,tipoOperacion="Registro",fecha="08/11/17";
	string resp;
	string tipo,nombre,unidad,stock,stockS,precioCompra,precioVenta;
	MostrarListaProductos();
	cout<<"Ingrese codigo ==> ";cin>>n; cout<<endl;
	if(ValidarCodigo(n)==false){
		cout<<"Ingrese tipo de producto: ";	cin>>tipo; cout<<endl;
		cout<<"Descripcion: ";cin>>nombre; cout<<endl;
		cout<<"Unidad del producto: ";	cin>>unidad; cout<<endl;
		cout<<"Stock del producto: ";	cin>>stock; cout<<endl;
		cout<<"Stock de seguridad: ";	cin>>stockS; cout<<endl;
		cout<<"PC/unitario: ";	cin>>precioCompra; cout<<endl;
		cout<<"PV/unitario: ";	cin>>precioVenta; cout<<endl;
		cout<<"Confirmar? ==> ";cin>>resp;
		if(resp=="s"){
			CargarDatosProducto(n,tipo,nombre,unidad,stock,stockS,precioCompra,precioVenta);
		    int i=1;
			CargarOperacion(tipoOperacion,n,fecha,i);
		}
	}else{
		menuBodeguero();
	}
	menuBodeguero();
}
void MostrarListaProductos(){
	 cout<<"cod 	tipo	   nombre	 capacidad  stock  precioCompra  precioVenta  "<<endl;
	 cout<<"100 	bebida	   cielo	 500ml      20     1.50          2 "<<endl;
	 cout<<"123 	bebida	   crush	 500ml      30     2.10          2.5"<<endl;
	 cout<<"254	    dulces	   morocha	 500gr      50     0.30          1"<<endl;
}
bool ValidarCodigo(string n){
	bool existe=true;
	int v=1;
	if(v==0){
		existe=true;
	}else{
		existe=false;
	}
	return existe;

}
void CargarDatosProducto(string codigo,string tipo,string nombre, string unidad,string stock,string stockS,string precioCompra,string precioVenta){
	 cout<<"cod 	tipo	   nombre	 capacidad  stock  precioCompra  precioVenta  "<<endl;
	 cout<<"100 	bebida	   cielo	 500ml      20     1.50          2 "<<endl;
	 cout<<"123 	bebida	   crush	 500ml      30     2.10          2.5"<<endl;
	 cout<<"254	    dulces	   morocha	 500gr      50     0.30          1"<<endl;
	 cout<<codigo<<"         "<<tipo<<"   "<<nombre<<"   "<<unidad<<"     "<<stock<<"    "<<precioCompra<<"     "<<precioVenta<<endl;
}
void CargarOperacion(string tipoOp,string codigo,string fecha,int i){
	if(i==1){
	cout<<" Tipo      codigo   fecha"<<endl;
	cout<<tipoOp<<"   "<<codigo<<"   "<<fecha<<endl;
	}
	if(i==2){
	cout<<" Tipo      codigo   fecha"<<endl;
	cout<<" Registro  352      08/11/17";cout<<endl;
	cout<<tipoOp<<"   "<<codigo<<"   "<<fecha<<endl;
	}
	if(i==3){
		cout<<" Tipo        codigo    fecha"<<endl;
		cout<<" Registro    352       08/11/17"<<endl;
		cout<<" Actualizar  352       08/11/17"<<endl;
	}


}
void ActualizarProducto(){
	string n,campo,tipoOp="Actualizar",fecha="08/11/17";
	cout<<"Ingresar Codigo ==> ";cin>>n;cout<<endl;
	if(!ValidarCodigo(n)){
		mostrarProductosBodega();
		cout<<"Ingrese el campo a actualizar ==> ";cin>>campo;
		modificarCampo(campo);
		CargarOperacion(tipoOp,n,fecha,2);

	}
	menuBodeguero();
}
void mostrarProductosBodega(){
	cout<<"cod 	tipo	   nombre	 capacidad  stock  precioCompra  precioVenta  "<<endl;
	cout<<"100 	bebida	   cielo	 500ml      20     1.50          2.00 "<<endl;
	cout<<"123 	bebida	   crush	 500ml      30     2.10          2.50"<<endl;
	cout<<"254	dulces	   morocha	 500gr      50     0.30          1.00"<<endl;
	cout<<"352  menestras  lentejas  1 kg       15     1.80          2.40"<<endl;
}
void modificarCampo(string campo){
	string cod,tipo,nombre,unidad,stock,precioCompra,precioVenta;
	if(campo=="codigo"){
		cout<<"Ingrese el nuevo codigo : ";cin>>cod;cout<<endl;
		cargarCampo(campo,cod);
	}
	if(campo=="tipo"){
			cout<<"Ingrese el nuevo tipo : ";cin>>tipo;cout<<endl;
			cargarCampo(campo,tipo);
	}
	if(campo=="nombre"){
			cout<<"Ingrese el nuevo nombre : ";cin>>nombre;cout<<endl;
			cargarCampo(campo,nombre);
	}
	if(campo=="stock"){
			cout<<"Ingrese el nuevo stock : ";cin>>stock;cout<<endl;
			cargarCampo(campo,stock);
	}
	if(campo=="precioCompra"){
			cout<<"Ingrese el nuevo precio de compra : ";cin>>precioCompra;cout<<endl;
			cargarCampo(campo,precioCompra);
	}
	if(campo=="precioVenta"){
			cout<<"Ingrese el nuevo precio de venta : ";cin>>precioVenta;cout<<endl;
			cargarCampo(campo,precioVenta);
	}
}

void cargarCampo(string campo,string valor){
	cout<<"cod 	tipo	   nombre	 capacidad  stock  precioCompra  precioVenta  "<<endl;
	cout<<"100 	bebida	   cielo	 500ml      20     1.50          2.00 "<<endl;
	cout<<"123 	bebida	   crush	 500ml      30     2.10          2.50"<<endl;
	cout<<"254	dulces	   morocha	 500gr      50     0.30          1.00"<<endl;
	cout<<"352  menestras  "<<valor<<"  1 kg       15     1.80          2.40"<<endl;
}

void ConsultaRegistro(){
	int opcion;
	cout<<"(1) Mostrar Cambios"<<endl;
	cout<<"(2) Filtro "<<endl;
	cout<<"(3) Regresar"<<endl;
	cout<<"Escoja una opcion ==> ";cin>>opcion;cout<<endl;
	string ti,cod,fe;
	switch(opcion){
	case 1: CargarOperacion("consulta","100","10",3);break;
	case 2: int op;
		    cout<<"(1) Tipo "<<endl;
	        cout<<"(2) Codigo "<<endl;
	        cout<<"(3) Fecha "<<endl;
	        cout<<"Escoja una opcion ==> ";cin>>op;cout<<endl;

	        switch(op){
	        case 1:
	        	    cout<<"Ingrese el tipo de operacion ==> ";cin>>ti;cout<<endl;
	        	    CargarOperacion(ti,"352","08/11/17",1);break;
	        case 2:
    	             cout<<"Ingrese el codigo del producto ==> ";cin>>cod;cout<<endl;
    	             CargarOperacion("Registro",cod,"08/11/17",3);break;
	        case 3:
    	             cout<<"Ingrese la fecha ==> ";cin>>fe;cout<<endl;
    	             CargarOperacion("Registro","352",fe,3);break;
	        default: ConsultaRegistro();

	        };break;
	case 3:  menuBodeguero();break;
	default: cout<<"OPCION NO VALIDA";
	}
	menuBodeguero();
}

void ConsultaProductos(){
	int opcion;
	string filtro="nada",tipo="Bebidas",fecha,nombre="morocha";
	cout<<"(1)Mostrar operaciones"<<endl;
	cout<<"(2)Filtro"<<endl;
	cout<<"(3)Regresar"<<endl;
	cin>>opcion;
	if(opcion==1){
		mostrarProductosBodega();

	}
	if(opcion==2){
		cout<<"Buscar por: "<<endl;
		cout<<"(1) Tipo"<<endl;
		cout<<"(2) Nombre"<<endl;
		int a;
		cin>>a;

		if(a==1){
			cout<<"Tipo: "<<tipo<<endl;
			mostrarProductos(tipo);
		}

		if(a==2){
					cout<<"Nombre"<<nombre;
					mostrarPro(nombre);
				}
	}
	if(opcion==3){
		menuBodeguero();
	}
	menuBodeguero();

}
void ConsultaVenta(){
	int opcion;
	string filtro="nada",tipo,fecha="08/11/17",user="davidAg";
	cout<<"(1)Mostrar operaciones"<<endl;
	cout<<"(2)Filtro"<<endl;
	cout<<"(3)Regresar"<<endl;
	cin>>opcion;
	if(opcion==1){
		string cod="producto";
		int cant=3;
		int i=3;

		cout<<"Fecha: 08/11/17"<<endl;
		mostrarReporteCompra(cod,cant,i);
	}
	if(opcion==2){
		cout<<"Buscar por: "<<endl;
		cout<<"(1) Codigo"<<endl;
		cout<<"(2) Usuario"<<endl;
		int a;
		cout<<"Escoja opcion ==> ";cin>>a;

		if(a==1){tipo="Fecha";
			cout<<"Fecha"<<fecha;
			mostrarVenta(tipo,fecha);
		}
		if(a==2){tipo="Usuario";
			cout<<"Usuario"<<user;
			mostrarVenta(tipo,user);
		}
	}
	if(opcion==3){
		menuBodeguero();
	}
	menuBodeguero();
}
void mostrarVenta(string tipo,string valor){
	if(tipo=="Fecha"){
		cout<<"Fecha    Monto "<<endl;
		cout<<valor <<"   7   "<<endl;
	}
	if(tipo=="Usuario"){
		cout<<"Usuario    Monto "<<endl;
		cout<<valor<<"    7     "<<endl;
	}
}

void ModificarDatosBodega(){

}
