//============================================================================
// Name        : st202v-entregable4.cpp
// Author      : Proyecto EzMarket
// Version     : 1.0
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

//============================================================================

const string ARCHIVO_BODEGA_VENTA_PRODUCTO = "filebodega.txt";
const string ARCHIVO_PRODUCTOS = "fileproductos.txt";
const string ARCHIVO_CLIENTE_COMPRA = "filecliente.txt";

// Administrador Rengo de Id_admin [00-99]
// Cliente Rengo de Id_Cliente [100-999]
// Bodeguero Rengo de Id_Bodeguero [1000-...]
const string ARCHIVO_ADMINISTRADOR_ADMIN_BODEG_CLIEN = "fileadmin.txt";

// Archivo temporal
const string ARCHIVO_TEMPORAL_COMPRAS = "filetemporalcompras.txt";
const char ARCHIVO_CARACTER =',';
const int CANT_REG_CABECERA_TEMP_COMP = 0;

//============================================================================

//Estructura de un producto de bodeguero
struct producto{
	string codigo;
	string marca;
	string tipo;
	string nombre;
	string unidad;
	string stock;
	string pventa;
	string pcompra;
};
struct operacion{
	int tipo;
	int codigo;
	string fecha;
	string hora;
};
struct filtroVenta{
	int tipo;
	int codigo;
	string fecha;
	string hora;
	string usuario;
};

bool validarCodigo(string cod, string nombreArchivo);
void cargarDatosNuevoProducto(producto p1);
// void cargarOperacion(operacion n);
void mostrarRegistro(string f);
void mostrarVenta(string f);
void mostrarProductos(string f);
void RegistrarNuevoProducto();
void consultarRegistro(	);
void consultarVenta();
void consultarProductos();
void salirSesion();
// void modificarDatosBodeguero();

struct Administrador{
	string usuario;
	string nombre;
	string apellido;
	string sexo;
	string correo;
	string direccion;
	string celular; /**/
	string contrasenia;
};

struct Administrador admin;

struct clientes{
	string usuario;
	string nombre;
	string apellido;
	string sexo;
	string correo;
	string direccion;
	string celular;
	string contrasenia;
};

struct clientes usuario;

struct bodega{
	string usuario;
	string nombre;
	string apellido;
	string nombreBodega;
	string ruc;
	string sexo;
	string correo;
	string direccion;
	string celular;
	string contrasenia;
	void inicializar(vector <string> datos) {
		usuario = datos[0];
		nombre = datos[1];
		apellido = datos[2];
		nombreBodega = datos[3];
		ruc = datos[4];
		sexo = datos[5];
		correo = datos[6];
		direccion = datos[7];
		celular = datos[8];
		contrasenia = datos[9];
	}
};

struct bodega bode;

struct registroCompra {
	string codigo;
	string tipo;
	string marca;
	string nombre;
	string unidad;
	string precio;
	string cantidad;
	string monto;
	string fecha;
	string hora;
	string bodega;

	void inicializar(vector <string> dat) {
		codigo = dat[0];
		tipo = dat[1];
		marca = dat[2];
		nombre = dat[3];
		unidad = dat[4];
		precio = dat[5];
		cantidad = dat[6];
		monto = dat[7];
		fecha = dat[8];
		hora = dat[9];
		bodega = dat[10];
	}
};

struct productoGeneral {
	// TIPO       CODIGO  MARCA   NOMBRE               UND
	// PERFUMERIA  000001  DOVE  JABON_EXOFOLIANTE  PACK_3_UN
	string tipo;
	string codigo;
	string marca;
	string nombre;
	string unidad;
	void inicializar(vector<string>datos) {
		tipo = datos[0];
		codigo = datos[1];
		marca = datos[2];
		nombre = datos[3];
		tipo = datos[4];
	}
};

void consultarCompras();
void mostrarCompras();
void mostrarCompras(string filtro);
void crearArchivoTipoProducto(int i);
void solicitarProducto();
void mostrarProductosPorTipo(string filtroS);
void mostrarTipoProducto();
// void crearArchivoListaTotalDeProductos();
/// void crearArchivosDeProductosBodegas();
void mostrarProductoDisponible(string codigo);
void cargarReporteCompraTemp(string user,string bodega,string codigo,string cantidad); // optimizar
// void cargarReporteVentaTemp(string user,string bodega,string codigo,string cantidad); // optimizar

void finalizarCompra();
void modificarDatosCliente();
void menuBodeguero();
void menuCliente();
void mostrarReporteCompra();
void imprimirDatos(vector<struct registroCompra> datos);
void modoDePago(int modo);
int suma_cif(int y);/////
bool validarTarjeta(long long n);
bool realizarCompra();
void cargarTempRealCompra();
void borrarTemp(string user);
void cargarTempRealVenta();

vector<string> leerLineaArchivoTempComp(string linea, char separador);
struct registroCompra leerDatosRegistroCompra(string linea, char separador);
int preguntaFormaPago();

void salir();
void registrarCliente();
void registrarBodeguero();
void registrarUsuario();
vector<string> leerLineaDatos(string linea, char separador);
void iniciarSesion();
void MenuDeEntrada();


void menuAdministrador();
void mostrarUsuarios();
void reporteDeUsuarios();
void reportarCompra();
void reporteClientes();
void reportarVenta();
void reportarOperaciones();
void reporteBodeguero();

void buscarFiltro(int filtroI,string filtroS, string nombreArchivo);
// void modificarDatosAdmi();
void buscarFiltro(int filtroI,string filtroS, string nombreArchivo);

int main() {

	MenuDeEntrada();

	return 0;
}

void MenuDeEntrada(){

	cout<<"Bienvenido a EZ Market"<<endl;
	cout<<"(1)Ingresar"<<endl;
	cout<<"(2)Registrar"<<endl;
	cout<<"(3)Salir"<<endl;
	cout <<"Escoger la opcion: ";

	int m; cin>>m;
	switch(m){
	case 1: iniciarSesion(); break;
	case 2: registrarUsuario(); break;
	case 3: salir(); break;
	}
}

vector<string> leerLineaDatos(string linea, char separador){
	vector<string> valores;
	string aux="";
	for(int i=0;i<linea.size();i++){
		if(linea[i]==separador){
			valores.push_back(aux);
			aux="";
		}else{
			aux+=linea[i];
		}
	}
	valores.push_back(aux);
	return valores;
}

void iniciarSesion(){
 	vector <struct bodega> Bodega;
 	vector <struct clientes> Cliente;
 	vector <struct Administrador> Administrador;
 	struct Administrador AdministradorAux;
 	struct clientes ClienteAux;
 	struct bodega BodegaAux;
	ifstream archivo(ARCHIVO_ADMINISTRADOR_ADMIN_BODEG_CLIEN.c_str());
	string linea;
	vector<string> lineaArchivo;
 	while(getline(archivo,linea)) {

 		lineaArchivo = leerLineaDatos(linea,',');
 		int n = atoi(lineaArchivo[0].c_str());

 		if(0 <= n and n < 100) {
 			AdministradorAux.usuario = lineaArchivo[0].c_str();
 			AdministradorAux.nombre = lineaArchivo[1].c_str();
 			AdministradorAux.apellido = lineaArchivo[2].c_str();
			AdministradorAux.sexo = lineaArchivo[3].c_str();
			AdministradorAux.correo = lineaArchivo[4].c_str();
			AdministradorAux.celular = lineaArchivo[5].c_str();
			AdministradorAux.contrasenia = lineaArchivo[6].c_str();

			Administrador.push_back(AdministradorAux);
 		} else if(1000 <= n and n < 10000)  {
 			BodegaAux.usuario = lineaArchivo[0].c_str();
			BodegaAux.nombre = lineaArchivo[1].c_str();
			BodegaAux.apellido = lineaArchivo[2].c_str();
			BodegaAux.nombreBodega = lineaArchivo[3].c_str();
			BodegaAux.ruc = lineaArchivo[4].c_str();
			BodegaAux.sexo = lineaArchivo[5].c_str();
			BodegaAux.correo = lineaArchivo[6].c_str();
			BodegaAux.direccion = lineaArchivo[7].c_str();
			BodegaAux.celular = lineaArchivo[8].c_str();

			Bodega.push_back(BodegaAux);
 		} else if(100 <= n and n < 1000) {
 			ClienteAux.usuario = lineaArchivo[0].c_str();
 			ClienteAux.nombre = lineaArchivo[1].c_str();
			ClienteAux.apellido = lineaArchivo[2].c_str();
			ClienteAux.sexo = lineaArchivo[3].c_str();
			ClienteAux.correo = lineaArchivo[4].c_str();
			ClienteAux.direccion = lineaArchivo[5].c_str();
			ClienteAux.celular = lineaArchivo[6].c_str();
			ClienteAux.contrasenia = lineaArchivo[7].c_str();

			Cliente.push_back(ClienteAux);
 		}
 	}
 	archivo.close();

 	string correo;
 	string contra;
 	string tipo;
 	bool correcto=false;
 	while(!correcto){
 		cout<<endl;
 		cout<<"                  INICIAR SESION                   "<<endl;
 		cout<<"***************************************************"<<endl;
 		cout<< "Correo electronico: ";
 		cin >> correo;
 		cout<< "Contrasenia: ";
 		cin >> contra;
 		cout<<"***************************************************"<<endl;
 		cout<<endl;
 		for(int i=0; i< Cliente.size();i++){
 			if(Cliente[i].correo==correo && Cliente[i].contrasenia==contra){
 				usuario.usuario=Cliente[i].usuario;
 				usuario.nombre=Cliente[i].nombre;
 				usuario.apellido=Cliente[i].apellido;
 				usuario.sexo=Cliente[i].sexo;
 				usuario.correo=Cliente[i].correo;
 				usuario.direccion=Cliente[i].direccion;
 				usuario.celular=Cliente[i].celular;
 				usuario.contrasenia=Cliente[i].contrasenia;
 				tipo="cliente";
 				correcto=true;
 			}
 		}
 		if (!correcto){
 		for(int i=0; i< Bodega.size();i++){
 			if(Bodega[i].correo==correo && Bodega[i].contrasenia==contra){
 				bode.usuario=Bodega[i].usuario;
 				bode.nombre=Bodega[i].nombre;
 				bode.apellido=Bodega[i].apellido;
 				bode.nombreBodega=Bodega[i].nombreBodega;
 				bode.ruc=Bodega[i].ruc;
 				bode.sexo=Bodega[i].sexo;
 				bode.correo=Bodega[i].correo;
 				bode.direccion=Bodega[i].direccion;
 				bode.celular=Bodega[i].celular;
 				bode.contrasenia=Bodega[i].contrasenia;
 				tipo="bodeguero";
 				correcto=true;
 			}
 		}
 		}

 		if (!correcto){
 		for(int i=0; i< Administrador.size();i++){
 			if(Administrador[i].correo==correo && Administrador[i].contrasenia==contra){
 				admin.usuario=Administrador[i].usuario;
 				admin.nombre=Administrador[i].nombre;
 				admin.apellido=Administrador[i].apellido;
 				admin.sexo=Administrador[i].sexo;
 				admin.correo=Administrador[i].correo;
 				admin.celular=Administrador[i].celular;
 				admin.contrasenia=Administrador[i].contrasenia;
 				tipo="administrador";
 				correcto=true;
 			}
 		}
 		}
 		if (correcto){
 			cout<<"***** Se ha iniciado correctamente *****"<<endl<< endl;
 		}else{
 			cout << "***** La contrasena o el correo esta mal. *****"<< endl;
 			cout << "***** Vuelve a intentarlo *****"<< endl;
 		}
 	}
 	if (tipo=="cliente"){
 		menuCliente();
 	}
 	if (tipo=="bodeguero"){
 		menuBodeguero();
 	}
 	if (tipo=="administrador"){
 		menuAdministrador();
 	}
}
void registrarUsuario(){
	cout<<"Registro de Usuario"<<endl;
	cout<<"Rol: "<<endl;

	cout << "[1]. cliente" << endl;
	cout << "[2]. bodeguero" << endl;

	string rol;
	cin>>rol;
	cout<<endl;
	if(rol=="cliente" or rol == "1"){
		registrarCliente();
	}
	if(rol=="bodeguero" or rol == "2"){
		registrarBodeguero();
	}
}

/*******CLIENTE**********/
void registrarCliente(){
	string t;
	ofstream archivo ;
	string m,n;
	cout<<"\t\tREGISTRO DE CLIENTE"<<endl;

	archivo.open (ARCHIVO_ADMINISTRADOR_ADMIN_BODEG_CLIEN.c_str(),ios::app);
	// archivo.open ("registro de cliente.txt",ios::app);

	cout<<"Nombre de Usuario:"<<endl; cout<<"\t\t"; cin>>m; getline(cin,n);
		n+=m; archivo <<n<<",";
		cout<<"Nombres:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"apellido:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"Sexo:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"Correo Electronico:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"Direccion:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"Numero de Celular:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"Contrasenia:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n;
		archivo<<endl;
		cout<<"Registrarse?"<<endl; cin>>t;

		if (t=="si") menuCliente();
		if(t=="no") registrarUsuario();

	archivo.close();
}
void registrarBodeguero(){
	string t;
	ofstream archivo ;
	string m,n;
	cout<<"\t\tREGISTRO DE BODEGUERO"<<endl;
	archivo.open (ARCHIVO_ADMINISTRADOR_ADMIN_BODEG_CLIEN.c_str() ,ios::app);
		cout<<"Nombre de Usuario:"<<endl; cout<<"\t\t"; cin>>m; getline(cin,n);
		n+=m;
		archivo <<n<<",";
		cout<<"Nombres:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"apellido:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"Nombre de la Bodega:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"RUC:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"Sexo:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"Correo Electronico:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"Direccion:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"Numero de Celular:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n<<",";
		cout<<"Contrasenia:"<<endl; cout<<"\t\t"; getline(cin,n);
		archivo <<n;
		archivo<<endl;
		cout<<"Registrarse?"<<endl;
		cin>>t;
		if (t=="si"){
			menuBodeguero();
		}
		if(t=="no"){
			registrarUsuario();
		}
	archivo.close();
}

void salir(){
	cout<<"gracias por su visita a EZ MARKET";
}

void menuBodeguero(){
    int a;
	cout<<"Menu del Bodeguero!!!"<<endl;
	cout<<" (1) Registrar Nuevo Producto."<<endl;
	cout<<" (2) Consultar Registro."<<endl;
	cout<<" (3) Consultar Ventas."<<endl;
	cout<<" (4) Consultar Productos."<<endl;
	cout<<" (5) Salir Sesion"<<endl;
	cout<<" Ingrese opcion: ";
	cin>>a;cout<<endl;
	switch(a){
		case 1:RegistrarNuevoProducto();break;
		case 2:	consultarRegistro(); break;
		case 3:	consultarVenta(); break;
		case 4:	consultarProductos(); break;
		case 5:MenuDeEntrada();break;
		default: MenuDeEntrada();
	}
}
void menuCliente(){

	cout<<"OPCIONES"<<endl;
	cout<<"(1) Consulta De Compras"<<endl;
	cout<<"(2) Solicitar Producto"<<endl;
	cout<<"(3) Finalizar Compra"<<endl;
	cout<<"(4) Cerrar Sesion"<<endl;
	int m;
	cout<<"Escoja una opcion ==> ";cin>>m;
	switch(m){
	case 1:
		consultarCompras();break;
	case 2:
		solicitarProducto();break;
	case 3:
		finalizarCompra();break;
	case 4:
		MenuDeEntrada();break;
	default:
		cout<<"opcion no valida"<<endl;
		MenuDeEntrada();
	}

}

void consultarCompras(){
	string filtro;
		int a;
		cout<<"***Consulta de compras***"<<endl;
		cout<<"[1] Compras Realizadas" << endl;
		cout<<"***Consulta de compras por***"<<endl;
		cout<<"[2] Codigo"<<endl;
		cout<<"[3] Fecha"<<endl;
		cout<<"[4] Hora"<<endl;
		cout<<"[5] Tipo"<<endl;
		cout<<"[6] Bodega"<<endl;
		cout<<"[7] Volver"<<endl;
	    cout<<"Ingrese filtro de busqueda: ";
	    cin>>a;cout<<endl;
	    switch(a){
	    case 1: mostrarCompras(); break;
	    case 2: cout<<"Ingrese Codigo: ";cin>>filtro;
	    mostrarCompras(filtro);break;
	    case 3: cout<<"Ingrese Fecha: ";cin>>filtro;
	    mostrarCompras(filtro);break;
	    case 4: cout<<"Ingrese Hora: ";cin>>filtro;
	    mostrarCompras(filtro);break;
	    case 5: cout<<"Ingrese Tipo: ";cin>>filtro;
	    mostrarCompras(filtro);break;
	    case 6: cout<<"Ingrese Bodega: ";cin>>filtro;
	    mostrarCompras(filtro);break;
	    default: ;menuCliente();
	    }
}

void mostrarCompras() { // Recuerda que debes de leerlo del archivo Cliene
	ifstream archivo(ARCHIVO_CLIENTE_COMPRA.c_str(), ios::in);
	string linea;
	vector<string> lineaAux;
	cout << "CODIGO  " << "TIPO           " << "MARCA   " << "NOMBRE      "<< "UND " << "CANT " << "MONTO " << "FECHA " << " HORA "<< endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	while(getline(archivo,linea)) {
		lineaAux = leerLineaDatos(linea,ARCHIVO_CARACTER);
		string aux = lineaAux[0].c_str();
		if(aux == usuario.usuario) {

			cout << lineaAux[5].c_str() << " "; // Nombre
			cout << lineaAux[4].c_str() << " "; // Marca
			cout << lineaAux[3].c_str() << " "; // Tipo
			cout << lineaAux[6].c_str() << " "; // Unidad
			cout << lineaAux[8].c_str() << " "; // Cantidad
			cout << lineaAux[10].c_str() << " "; // fecha
			cout << endl;
		}
	}
	archivo.close();
	cout << endl; consultarCompras();
}
void mostrarCompras(string filtro){

	ifstream archivo(ARCHIVO_CLIENTE_COMPRA.c_str(), ios::in);
	vector<string> datos;
	string linea;
	string aux1,aux2;
	bool sentinel = true;
	if(archivo.is_open()) {
		while(!archivo.eof()) {
			getline(archivo,linea);
			datos = leerLineaDatos(linea,ARCHIVO_CARACTER);
			aux1 = datos[0].c_str();
			if(aux1 == usuario.usuario) {
				for(int i = 0; i < datos.size(); i++) {
					aux2 = datos[i].c_str();
					if(filtro == aux2) {
						if(sentinel) {
							cout << "NOMBRE " << " MARCA " << " TIPO " << " UND " << " CANTIDAD " << " FECHA " << endl;
							sentinel = false;
						}
						// 169 000016 LIMPIEZA AYUDIN LAVAVAJILLAS 310_GR 3 3 9 fecha hora 1000
						cout << datos[5].c_str() << " "; // Nombre
						cout << datos[4].c_str() << " "; // Marca
						cout << datos[3].c_str() << " "; // Tipo
						cout << datos[6].c_str() << " "; // Unidad
						cout << datos[8].c_str() << " "; // Cantidad
						cout << datos[10].c_str() << " "; // fecha
						cout << endl;
					}
				}
			}
		}
	} else {
		cout << "Usted no ha realizado pedidos aun, es decir, no nos ha comprado todavia" << endl;
		menuCliente();
	}
	archivo.close();
	consultarCompras();
}

void solicitarProducto(){
	cout<<"Ubicacion actual:"<<endl;
	cout<<"(1) Domicilio"<<endl;
	cout<<"(2) Nueva ubicacion"<<endl;
	int o;cout<<"Escoja opcion ==> ";cin>>o;
	string ub;
	switch(o){
	case 1: ub=usuario.direccion; break;
	case 2: cout<<"Ingrese direccion : ";cin>>ub;break;
	default: cout<<"Opcion no valida"<<endl;
		menuCliente();
	}

	string cod;
	cout<<" Buscar por :"<<endl;
	cout<<" [1]. Tipo de producto"<<endl;
	cout<<" [2]. Codigo del producto"<<endl;
	cout<<" [3]. Regresar "<<endl;
	int op;
	cout<<"Escoja opcion ==> ";cin>>op;
	switch (op){
	case 1:
		mostrarTipoProducto();break;
	case 2:
		cout<<"Ingrese el codigo del producto = ";cin>>cod;
		mostrarProductoDisponible(cod);break;
	case 3:
		menuCliente();break;
	default:
		cout<<"opcion no valida"<<endl;
		menuCliente();
	}
}

void crearArchivoTipoProducto(int i){
	string t;
		ofstream archivo ;
		string m,n;
		archivo.open ("tipos de producto.txt",ios::app);
		if(i==1){

			m="PERFUMERIA";
			n+=m;
			archivo <<n<<endl;
			n="LIMPIEZA";
			archivo <<n<<endl;
			n="BEBIDAS-CERVEZAS";
			archivo <<n<<endl;
			n="DESAYUNOS-DULCES";
			archivo <<n<<endl;
			n="QUESOS-FIAMBRES";
			archivo <<n<<endl;
			n="LACTEOS-HUEVOS";
			archivo <<n<<endl;
			n="FRUTAS-VERDURAS";
			archivo <<n<<endl;
			n="ABARROTES";
			archivo <<n;
		}else{
			cout<<"\t\tREGISTRO DE TIPO DE PRODUCTO"<<endl;
					cout<<"Nombre del nuevo tipo de producto:"<<endl;
					cout<<"\t\t";
					cin>>m;
					getline(cin,n);
					n+=m;
					archivo <<n<<endl;
					cout<<"Registrarse? ==> ";cin>>t;
					if (t=="si"){
								menuBodeguero();
							}
					if(t=="no"){
								RegistrarNuevoProducto();
							}
		}
		archivo.close();
}
void mostrarProductosPorTipo(string filtroS){
	//string nombreArchivo1="lista general de productos.txt";
	ifstream archivo1(ARCHIVO_PRODUCTOS.c_str());
	string linea,campo1;
	int i;
	if(archivo1.is_open()){
		while(!archivo1.eof()){
		  getline(archivo1,linea);
		  stringstream s(linea);
			while(!s.eof()){
				getline(s,campo1,',');
				if(campo1==filtroS){
					for(i=2;i<=5;i++){
						getline(s,campo1,',');
						cout<<campo1<<" ";
					}cout<<endl;
				}
			}
		}
	}
	archivo1.close();
}

void mostrarTipoProducto(){
	// PERFUMERIA,000001,DOVE,JABON_EXOFOLIANTE,PACK_3_UN
	ifstream archivo(ARCHIVO_PRODUCTOS.c_str());
	string linea;
	string aux;
	vector<string> datosAux; // Guardo los tipos de producto
	vector<string> datos;
	int i = 0;
	bool primero = true;
	if(archivo.is_open()){
		while(getline(archivo,linea)){
			datos = leerLineaDatos(linea,ARCHIVO_CARACTER);// si lo leo de archivo file productos
			linea =  datos[0].c_str();
			if(primero) {
				datosAux.push_back(linea);
				primero = false;
			} else {
				for( i = 0; i < datosAux.size(); i++) {
					if(linea != aux) {
						datosAux.push_back(linea);
						aux = linea;
						break;
					} else if(linea == datosAux[i].c_str()) break;
				}
			}
		}
		for(int i = 1; i < datosAux.size(); i++) {
			cout << " ["<< i << "] "<< datosAux[i].c_str() << endl;
		}
	archivo.close();
	cout<<"["<<i+1<<"] Regresar"<<endl;
	int opcion;
	cout<<"Escoja opcion ==> ";cin>>opcion;

	cout<<"CODIGO     MARCA    DESCRIPCION    UNIDAD"<<endl;
	string cod, resp, filtro;
	filtro = datosAux[opcion];

	mostrarProductosPorTipo(filtro);
	cout<<"\nIngrese el codigo del producto ==> ";cin>>cod;
	mostrarProductoDisponible(cod);
	cout<<"Desea solicitar otro producto? (S/N) ==> ";cin>>resp;
		if(resp=="S" || resp=="s"){
		solicitarProducto();
		}else{
		menuCliente();
		}
	}else{
	cerr<<"Por el momento no tenemos productos a mostrar, estamos trabajano en ello";
	menuCliente();
	}
}

void mostrarProductoDisponible(string codigo){
	// file Admin
	// 1000,ingrid olenka,arriaga valle,TIO BENITO,1245236980,F,ingrid.0797@gmail.com,av.larco,985654325,10214
	// file bodegaProductos
	// 1000,P,000016,LIMPIEZA,AYUDIN,LAVAVAJILLAS,310_GR,30,2.6,3
	/*cout<<"  **********************BODEGA TIO BENITO - 1000**************"<<endl;
	cout<<"Codigo            Marca              Descripcion               Stock        Precio  "<<endl;*/
	ifstream lectura(ARCHIVO_ADMINISTRADOR_ADMIN_BODEG_CLIEN.c_str());
	ifstream archivo(ARCHIVO_BODEGA_VENTA_PRODUCTO.c_str(), ios::in);
	vector<string> datoBodega;
	string lineaBodega;
	int n,m;
	struct bodega Bodega;
	vector<struct bodega> DatosBodega;
	string lineaAdmin;
	while(getline(lectura,lineaAdmin)) {
		vector<string> datos = leerLineaDatos(lineaAdmin, ARCHIVO_CARACTER);
		Bodega.usuario = datos[0].c_str();
		Bodega.nombreBodega = datos[3];
		m = atoi(datos[0].c_str());

		if(1000 <= m && m <= 10000) {
			DatosBodega.push_back(Bodega);
		}
	}
	while(!archivo.eof()) {
		getline(archivo,lineaBodega);
		datoBodega = leerLineaDatos(lineaBodega,ARCHIVO_CARACTER);
		string aux = datoBodega[2].c_str();
		if(codigo == aux) {
			n = atoi(datoBodega[7].c_str());
			if(n > 0) {

				for(int i = 0; i < DatosBodega.size(); i++) {
					string aux1 = datoBodega[0].c_str();
					if(aux1 == DatosBodega[i].usuario &&  datoBodega[1] == "P") {
						cout<<"  **********************" << DatosBodega[i].nombreBodega << " - "
								<< DatosBodega[i].usuario << "**************"<<endl;
						// Codigo            Marca              Descripcion               Stock        Precio
						cout << datoBodega[2].c_str() << " " << datoBodega[4].c_str() << " "
								<< datoBodega[3].c_str() << " " << datoBodega[7].c_str() << " "
								<< datoBodega[8].c_str() << endl;
					}
				}
			}
		}
	}
	archivo.close();
	lectura.close();
	string codbo;
	cout<<"Ingrese el codigo de la bodega ==> ";cin>>codbo;cout<<endl;
	string cant;
	cout<<"Ingrese la cantidad que desea comprar ==> ";cin>>cant;cout<<endl;
	cargarReporteCompraTemp(usuario.usuario,codbo,codigo,cant);
}

// 000016,LIMPIEZA,AYUDIN,LAVAVAJILLAS,310_GR,3,3,9,fecha,hora,1000 // temporal de compras
// 000001,PERFUMERIA,DOVE,JABON_EXOFOLIANTE,PACK_3_UN,40,6.5,7.2 // Archivo de donde leere
// 1000,P,000016,LIMPIEZA,AYUDIN,LAVAVAJILLAS,310_GR,30,2.6,3  // archivo A USAR // reco
// archivo<<codigoPoructo<<","<<campo1<<"," << cantidad<<","<<mont<<","<<"fecha"<<","<<"hora"<<","<<bodega<<endl;
void cargarReporteCompraTemp(string user,string bodega,string codigo,string cantidad){ // No use usuario

	string linea;
	vector <string> datos;
	float monto = atoi(cantidad.c_str());
	ifstream archivo(ARCHIVO_BODEGA_VENTA_PRODUCTO.c_str(), ios::in);
	ofstream escribir(ARCHIVO_TEMPORAL_COMPRAS.c_str(), ios::app);

	while(!archivo.eof()) {
		getline(archivo,linea);
		datos = leerLineaDatos(linea, ARCHIVO_CARACTER);
		string aux = datos[0].c_str();
		if(bodega == aux) {
			string aux2 = datos[2].c_str();
			if(codigo == aux2) {
				monto *= atof(datos[9].c_str());
				escribir << codigo << "," << datos[3].c_str() << "," << datos[4].c_str() << ","
						<< datos[5].c_str() << "," << datos[6].c_str() << "," << datos[8].c_str() << ","
						<< cantidad << "," << monto << "," << "fecha" << "," << "hora" << "," << bodega << endl;
			break;
			}
		}
	}
	escribir.close();
	archivo.close();
}

// 000016,LIMPIEZA,AYUDIN,LAVAVAJILLAS,310_GR,3,3,9,fecha,hora,169
// 000020,LIMPIEZA,ELITE,PAPEL_TOALLA_ULTRA,PAQUETE,3,1,3,fecha,hora,1000

void finalizarCompra(){
	mostrarReporteCompra();
	string resp;
	cout<<"Desea confirmar la compra?(S/N) ==> ";cin>>resp;
	if(resp =="S"||resp=="s"){
		modoDePago(preguntaFormaPago());
		cargarTempRealVenta();
		cargarTempRealCompra();
		//actualizarStock("1000");
		//actualizarStock("2000");
		//actualizarStock("3000");
		menuCliente();


	}else{
		/* Implementar para cuando se ciera la sesion
		 * cargarTempRealVenta();
		cargarTempRealCompra();*/
		menuCliente();
	}

}

void mostrarReporteCompra() {
	ifstream archivo(ARCHIVO_TEMPORAL_COMPRAS.c_str());
	string lectura;
	vector<struct registroCompra> Datos;
	float suma = 0;
	if(!archivo.is_open()) {
		cerr << "Usted aun no ha relizado sus compras" << endl;
	} else {
		for(int i = 0; i < CANT_REG_CABECERA_TEMP_COMP; i++) {
			getline(archivo, lectura);
		}
		while(getline(archivo,lectura)) {
			struct registroCompra datos = leerDatosRegistroCompra(lectura,ARCHIVO_CARACTER);
			Datos.push_back(datos);
			suma  += atof(datos.monto.c_str());
		}
		cout << "MARCA " << " NOMBRE " << " UNIDAD " << " PRECIO " << " CANTIDAD "  << " MONTO " << endl;
		imprimirDatos(Datos);
		cerr << "                         Monto de la Compra:" << suma << endl;
	}
}
void imprimirDatos(vector<struct registroCompra> datos) {
	for(int i = 0; i < datos.size(); i++) {
		// 000016,LIMPIEZA,AYUDIN,LAVAVAJILLAS,310_GR,3,3,9,fecha,hora,/*num Bodega*/
		cout << datos[i].marca << " ";
		cout << datos[i].nombre << " ";
		cout << datos[i].unidad << " ";
		cout << datos[i].precio << " ";
		cout << datos[i].cantidad << " ";
		cout << datos[i].monto << " " << endl;
	}
}
void modoDePago(int modo) {
	long long numTarjeta;
	if(modo == 1) {
		cout << "Digite su numero de tarjeta "; cin >> numTarjeta;
		if(validarTarjeta(numTarjeta)) {
			cout<<"TARJETA VALIDADA....COMPRA REALIZADA CON EXITO"<<endl;
			cout<<"Se esta enviando el pedido"<<endl;

		}else{
			modoDePago(1);
		}

	} else if(modo == 2) {
		cout<<"COMPRA CONFIRMADA"<<endl;
		cout<<"Se esta enviando el pedido"<<endl;


	} else {
		cout << "Esa opcion no esta registrada";
		preguntaFormaPago();
	}
}
int preguntaFormaPago() {
	int modo;
	cout << "Uted desea pagar con: " << endl;
	cout << "[1] Tarjeta" << endl;
	cout << "[2] Efectivo" << endl;
	cout << "                  Digite la opcion!!!"; cin >> modo;
	return modo;
}
int suma_cif(int y){
	int s,d,u;
	u = y%10;
	d = y/10;
	return s=u+d;
}
bool validarTarjeta(long long n) {
	bool existe;
	long long num[17];
	int imp[9],par[9],mul[9];
	int r=0;
	for(int i=0;i<16;i++){
		num[i]=n/(pow(10,15-i));
		if(i!=15){
			n-=num[i]*pow(10,15-i);
		}
	}
	for(int i=0;i<8;i++){
		imp[i]=num[i*2];
		par[i]=num[i*2+1];
		mul[i]=par[i]*2;
		if (mul[i]>9){
			mul[i]=suma_cif(mul[i]);
		}
		r= r+imp[i]+mul[i];
	}
	if(r%10==0){
		existe=true;
	}else{
		existe=false;
	}
	return existe;
}
bool realizarCompra() { // ?
	string aux;
	cout << "Confimar la compra de productos: " << endl;
	cout <<" Si/No " << endl; cin >> aux;
	if(aux == "Si" or aux == "SI" or aux == "si") return true;
	else return false;

	return false;
}
void cargarTempRealCompra() {

	string aux;
	ofstream archivo(ARCHIVO_CLIENTE_COMPRA.c_str(), ios::app);
	ifstream lectura(ARCHIVO_TEMPORAL_COMPRAS.c_str(), ios::in);
	if(!lectura.is_open()) {
		cerr << "Espere un momento ... " << endl;
		cout << "Sie el problema persiste llame a Ez Market!!" << endl;
	} else {
		while(getline(lectura,aux)){
			archivo << usuario.usuario << "," << aux << endl;
		}
	}
	lectura.close();
	archivo.close();
	remove(ARCHIVO_TEMPORAL_COMPRAS.c_str());

}

void borrarTemp(string nombre) {
	remove(nombre.c_str());
}
void cargarTempRealVenta() {
	ofstream _file(ARCHIVO_BODEGA_VENTA_PRODUCTO.c_str(), ios::app);
	ifstream _read(ARCHIVO_TEMPORAL_COMPRAS.c_str());
	string linea;
	vector<string> datos;
	while(getline(_read,linea)) {
		datos = leerLineaDatos(linea,ARCHIVO_CARACTER);
		_file << datos[10].c_str() << ",V," << datos[0].c_str() << ","<< datos[1].c_str() << ","
				<< datos[2].c_str() << ","<< datos[3].c_str() << ","<< datos[4].c_str() << ","
				<< datos[5].c_str() << ","<< datos[6].c_str() << ","<< datos[7].c_str() << ","
				<< datos[8].c_str() << ","<< datos[9].c_str() << ","<< usuario.usuario << endl;
	}
}

vector<string> leerLineaArchivoTempComp(string linea, char separador){
	vector<string> valores;
	string aux="";
	for(int i=0;i<linea.size();i++){
		if(linea[i]==separador){
			valores.push_back(aux);
			aux="";
		}else{
			aux+=linea[i];
		}
	}
	valores.push_back(aux);
	return valores;
}

struct registroCompra leerDatosRegistroCompra(string linea, char separador){
	vector<string> datos = leerLineaArchivoTempComp(linea,separador);
	struct registroCompra respuesta;
	respuesta.inicializar(datos);
	return respuesta;
}
void modificarDatosCliente() {};

//###########      Reporte de Usuarios        ###################
void buscarFiltro(int filtroI,string filtroS, string nombreArchivo){
			ifstream archivo1(nombreArchivo.c_str());
			string linea,campo1;
			int i=1;
			if(archivo1.is_open()){
				while(!archivo1.eof()){
					getline(archivo1,linea);
					stringstream s(linea);
					while(!s.eof() ){
						getline(s,campo1,',');
						if(i==filtroI){
							if(campo1==filtroS){
								cout<<linea<<endl;
							}
						}
							i++;
					}
					i=1;
				}
				archivo1.close();
			}else{
				cout<<"No se pudo abrir el archivo"<<endl;
			}
			menuAdministrador();
}
void mostrarUsuarios(){// ???
	int filtroI;
	string filtroS;

	int opcion;
	cin>>opcion;
	string nombreArchivo1="registro de cliente.txt";
	string nombreArchivo2="registro de bodeguero.txt";
	if(opcion==1){
		ifstream archivo1(nombreArchivo1.c_str());
		string linea,campo1;
		int i=1;
		cout<<"Escoja el campo que quiere mostrar"<<endl;
		cin>>filtroI;
		cout<<"Digite el filtro"<<endl;
		cin>>filtroS;
		if(archivo1.is_open()){
			while(!archivo1.eof()){
				getline(archivo1,linea);
				stringstream s(linea);
				while(!s.eof() ){
					getline(s,campo1,';');
					if(i==filtroI){
						if(campo1==filtroS){
							cout<<linea<<endl;
						}
					}
						i++;
				}
				i=1;
			}
			archivo1.close();
		}else{
			cout<<"No se pudo abrir el archivo"<<endl;
		}

	}else if(opcion==2){
		ifstream archivo2(nombreArchivo2.c_str());
		string linea,campo1;
		if(archivo2.is_open()){
			while(!archivo2.eof()){

			getline(archivo2,campo1,';');


		}
				archivo2.close();
		}else{
			cout<<"No se pudo abrir el archivo"<<endl;
		}
	}else{
		cout<<"Opcion ingresada no valida"<<endl;
	}

}
void reporteDeUsuarios(){
	int filtroI,opcion1;
	string nombreArchivo,filtroS;

	cout<<"OPCIONES"<<endl;
	cout<<"(1)Reporte de Clientes"<<endl;
	cout<<"(2)Reporte de Bodegueros"<<endl;
	cout<<"(3)Regresar"<<endl;
	cin>>opcion1;
	if(opcion1==1){
		cout<<"Buscar por:"<<endl;
		cout<<"(1)Codigo de Usuario"<<endl;
		cout<<"(2)Nombre"<<endl;
		cout<<"(3)Apellido"<<endl;
		cout<<"(4)Sexo"<<endl;
		cout<<"(5)Correo"<<endl;
		cout<<"(6)Direccion"<<endl;
		cout<<"(7)Telefono"<<endl;
		cin>>filtroI;

		cout<<"Digite lo que desea buscar"<<endl;
		cin>>filtroS;

		nombreArchivo="registro de cliente.txt";

		buscarFiltro(filtroI,filtroS,nombreArchivo);


	}else if(opcion1==2){
		cout<<"Buscar por: "<<endl;
		cout<<"(1)Codigo de Usuario"<<endl;
		cout<<"(2)Nombre"<<endl;
		cout<<"(3)Apellido"<<endl;
		cout<<"(4)Bodega"<<endl;
		cout<<"(5)Ruc"<<endl;
		cout<<"(6)Sexo"<<endl;
		cout<<"(7)Correo"<<endl;
		cout<<"(8)Direccion"<<endl;
		cout<<"(9)Telefono"<<endl;
		cin>>filtroI;

		cout<<"Digite lo que desea buscar"<<endl;
		cin>>filtroS;

		nombreArchivo="registro de bodegueros.txt";

		buscarFiltro(filtroI,filtroS,nombreArchivo);

	}else if(opcion1==3){
		system("cls");
		menuAdministrador();
	}else if(opcion1!=1 or opcion1!=2 or opcion1!=3){
		cout<<"Opcion ingresada no valida"<<endl;
		reporteDeUsuarios();
	}
}
//#############################################################


//##########       Reporte de Clientes     ####################
void reportarCompra(){
	int filtroI;
	string nombreArchivo,filtroS,codCliente;
	cout<<"Ingrese el codigo del cliente : ";cin>>codCliente;cout<<endl;


	cout<<"Buscar por: "<<endl;
	cout<<"(1)Codigo"<<endl;
	cout<<"(2)Tipo"<<endl;
	cout<<"(3)Nombre"<<endl;
	cout<<"(4)Fecha"<<endl;//fecha es el campo numero 8
	cout<<"(5)Hora"<<endl;//hora es el campo numero 9
	cout<<"(6)Regresar"<<endl;

	cin>>filtroI;
	if(filtroI==4){
		filtroI=8;
	}else if(filtroI==5){
		filtroI=9;
	}else if(filtroI==6){
		menuAdministrador();
	}
	if(filtroI>9){
		cout<<"Opcion ingresada no valida"<<endl;
		reportarCompra();
	}

	cout<<"Digite lo que desea buscar"<<endl;
	cin>>filtroS;

	nombreArchivo="compra cliente "+codCliente+".txt";

	buscarFiltro(filtroI,filtroS,nombreArchivo);
}

void reporteClientes(){
	reportarCompra();
}
//#############################################################


//############     Reporte de Bodegueros    ###################
void reportarVenta(){
		int filtroI;
		string nombreArchivo,filtroS,codbo;

		cout<<"Ingrese el codigo de la bodega : ";cin>>codbo;cout<<endl;

		cout<<"Buscar por: "<<endl;
		cout<<"(1)Codigo"<<endl;
		cout<<"(2)Tipo"<<endl;
		cout<<"(3)Nombre"<<endl;
		cout<<"(8)Fecha"<<endl;
		cout<<"(9)Hora"<<endl;

		cin>>filtroI;

		cout<<"Digite lo que desea buscar"<<endl;
		cin>>filtroS;

		nombreArchivo="venta bodega "+codbo+".txt";

		buscarFiltro(filtroI,filtroS,nombreArchivo);
}
void reportarOperaciones(){
	int filtroI;
	string nombreArchivo,filtroS;

	cout<<"Buscar por: "<<endl;
	cout<<"(1)Tipo"<<endl;
	cout<<"(2)Codigo"<<endl;
	cout<<"(3)Fecha"<<endl;
	cout<<"(4)Hora"<<endl;

	cin>>filtroI;

	cout<<"Digite lo que desea buscar"<<endl;
	cin>>filtroS;

	nombreArchivo="reporteOperaciones.txt";

	buscarFiltro(filtroI,filtroS,nombreArchivo);
}
void reporteBodeguero(){
	int opcion;
	cout<<"OPCIONES"<<endl;
	cout<<"(1)Reporte de ventas"<<endl;
	cout<<"(2)Reporte de Operaciones"<<endl;
	cout<<"(3)Regresar"<<endl;
	cin>>opcion;

	if(opcion==1){
		reportarVenta();
	}else if(opcion==2){
		reportarOperaciones();
	}else if(opcion==3){
		menuAdministrador();
	}
}

//##########     Menu de Administrador     ####################
void menuAdministrador(){
	cout<<"OPCIONES"<<endl;
	cout<<"(1) Reporte de Usuarios"<<endl;
	cout<<"(2) Reporte de Clientes"<<endl;
	cout<<"(3) Reporte de Bodeguero"<<endl;
	cout<<"(4) Salir Sesion"<<endl;
	int option;
	cout<<"Elija una opcion ==> ";cin>>option;cout<<endl;
	switch(option){
		case 1:
			reporteDeUsuarios();
			break;
		case 2:
			reporteClientes();break;
		case 3:
			reporteBodeguero();break;
		case 4:
			MenuDeEntrada();break;
		default:
			cout<<"Opcion no valida"<<endl;
			MenuDeEntrada();

		}
}

//Registrar Nuevo Producto
bool validarCodigo(string cod, string nombreArchivo){
	bool n=false;


		    ifstream archivo1(nombreArchivo.c_str());
			string linea,campo1;
			int i;
			if(archivo1.is_open()){
				while(!archivo1.eof()){
				  getline(archivo1,linea);
				  stringstream s(linea);
					while(!s.eof()){

							for(i=1;i<=5;i++){
								getline(s,campo1,',');
								if(i==2){
									if(campo1==cod){
										n=true;
									}


									}
							}

						     }


						}

				}
						archivo1.close();
						if(n==true){
							cout<<"CODIGO YA EXISTE"<<endl;
							menuBodeguero();
						}
						return n;
}
void cargarDatosNuevoProducto(producto p1){
    ofstream archivo;
	archivo.open("lista general de productos.txt",ios::app);
    archivo<<p1.tipo<<","<<p1.codigo<<","<<p1.marca<<","<<p1.nombre<<","<<p1.unidad<<endl;
    archivo.close();
    string arch="lista de productos bodega "+bode.usuario+".txt";
    ofstream archivo1(arch.c_str(),ios::app);
    archivo1<<p1.codigo<<","<<p1.tipo<<","<<p1.marca<<","<<p1.nombre<<","<<p1.unidad<<","<<p1.stock<<","<<p1.pcompra<<","<<p1.pventa<<endl;
    archivo1.close();
    string ar="registro de productos bodega "+bode.usuario+".txt";
    ofstream a(ar.c_str(),ios::app);
    a<<p1.codigo<<","<<p1.tipo<<","<<p1.nombre<<","<<"fecha"<<","<<"hora"<<endl;
    a.close();
}

//Consultar Registro
void mostrarRegistro(string f){
	string parch="registro de productos bodega "+bode.usuario+".txt";
	ifstream arch(parch.c_str());
	string linea,campo1;
			int i;
			if(arch.is_open()){
				while(!arch.eof()){
				  getline(arch,linea);
				  stringstream s(linea);
					while(!s.eof()){
						for(i=1;i<=5;i++){
							getline(s,campo1,',');
					     	if(campo1==f){
					  		cout<<linea<<endl;

						  }
						}

						     }


						}

		}
		arch.close();
  menuBodeguero();

}

//Consultar Venta
void mostrarVenta(string f){
	string parch="venta bodega "+bode.usuario+".txt";
	ifstream arch(parch.c_str());
	string linea,campo1;
		int i;
		if(arch.is_open()){
			while(!arch.eof()){
			  getline(arch,linea);
			  stringstream s(linea);
				while(!s.eof()){
					for(i=1;i<=11;i++){
						getline(s,campo1,',');
				     	if(campo1==f){
				     		cout<<linea<<endl;
				     	}
					}
			     }
			}
		}
		arch.close();
		menuBodeguero();
}

//Consultar Producto // codigo, tipo , nombre
void mostrarProductos(string f){
	string parch="lista de productos bodega "+bode.usuario+".txt";
	ifstream arch(parch.c_str());
	string linea,campo1;
		int i;
		if(arch.is_open()){
			while(!arch.eof()){
			  getline(arch,linea);
			  stringstream s(linea);
					while(!s.eof()){
						for(i=1;i<=8;i++){
							getline(s,campo1,',');
					    	if(campo1==f){
					    		cout<<linea<<endl;
					    	}
						}
				    }
				}
			}
	arch.close();
	menuBodeguero();
}

//Menu del Bodeguero
void RegistrarNuevoProducto(){
	string b;
    int a;
	producto x;
	string Inventario1;

	cout<<" (1) Registrar nuevo producto "<<endl;
	cout<<" (2) Volver "<<endl;
	cout<<" Ingrese opcion: ";cin>>a;cout<<endl;
	string nombreArchivo2 = "tipos de producto.txt";
	ifstream archio(nombreArchivo2.c_str());

	switch(a){
		case 1:
			cout<<"Ingrese el codigo del nuevo producto: ";cin>>x.codigo;
			if(!validarCodigo(x.codigo,"lista general de productos.txt")){

					if(archio.is_open()){
						string linea;
						int i=1;
						while(!archio.eof()){
						getline(archio,linea);
						cout<<"("<<i<<")"<<linea<<endl;
						i++;
						}
					archio.close();
					int w;
				cout<<"Ingrese el tipo de producto ==> ";cin>>w;cout<<endl;
				switch(w){
				case 1 : x.tipo="PERFUMERIA";break;
				case 2 : x.tipo="LIMPIEZA";break;
				case 3 : x.tipo="BEBIDAS-CERVEZAS";break;
				case 4 : x.tipo="DESAYUNOS-DULCES";break;
				case 5 : x.tipo="QUESOS-FIAMBRES";break;
				case 6 : x.tipo="LACTEOS-HUEVOS";break;
				case 7 : x.tipo="FRUTAS-VERDURAS";break;
				case 8 : x.tipo="ABARROTES";break;
				default: menuBodeguero();
				}
				cout<<"Marca: ";cin>>x.marca;cout<<endl;
				cout<<"Nombre: ";cin>>x.nombre;cout<<endl;
				cout<<"Unidad: ";cin>>x.unidad;cout<<endl;
				cout<<"Stock: ";cin>>x.stock;cout<<endl;
				cout<<"Precio de Compra: ";cin>>x.pcompra;cout<<endl;
				cout<<"Precio de Venta: ";cin>>x.pventa;cout<<endl;
				cargarDatosNuevoProducto(x);
				cout<<"PRODUCTO REGISTRADO CON EXITO"<<endl;
				menuBodeguero();
			}

				menuBodeguero();
			;break;

	   case 2:
		menuBodeguero();break;
   	default: menuBodeguero();
		}
	}
}
void consultarRegistro(){
	string filtro;
	int a;
    cout<<"Consulta de registro de nuevos productos"<<endl;
	cout<<"1. Codigo"<<endl;
	cout<<"2. Fecha"<<endl;
	cout<<"3. Hora"<<endl;
	cout<<"4. Volver"<<endl;
    cout<<"Ingrese filtro de bÃºsqueda: ";
    cin>>a;cout<<endl;
    switch(a){
        case 1:cout<<"Ingrese Codigo: ";cin>>filtro;
             mostrarRegistro(filtro);break;
        case 2:cout<<"Ingrese Fecha de realizacion: ";cin>>filtro;
             mostrarRegistro(filtro);break;
        case 3:cout<<"Ingrese Hora de realizacion: ";cin>>filtro;
             mostrarRegistro(filtro);break;
        default: menuBodeguero();
    }

}

void consultarVenta(){
	string filtro;
	int a;
	cout<<"***Consulta de ventas***"<<endl;
	cout<<"1. Codigo"<<endl;
	cout<<"2. Fecha"<<endl;
	cout<<"3. Hora"<<endl;
	cout<<"4. Tipo"<<endl;
	cout<<"5. Cliente"<<endl;
	cout<<"6. Volver"<<endl;
    cout<<"Ingrese filtro de bÃºsqueda: ";
    cin>>a;cout<<endl;
    switch(a){
    case 1:;cout<<"Ingrese Codigo: ";cin>>filtro;
    	mostrarVenta(filtro);break;
    case 2:;cout<<"Ingrese Fecha: ";cin>>filtro;
		mostrarVenta(filtro);break;
    case 3:;cout<<"Ingrese Hora: ";cin>>filtro;
		mostrarVenta(filtro);break;
    case 4:;cout<<"Ingrese Tipo: ";cin>>filtro;
		mostrarVenta(filtro);break;
    case 5:;cout<<"Ingrese Cliente: ";cin>>filtro;
		mostrarVenta(filtro);break;
    default: ;menuBodeguero();
    }
}
void consultarProductos(){
	string filtro;
		int a;
	    cout<<"***Consulta de productos***"<<endl;
	    cout<<"1. Codigo"<<endl;
		cout<<"2. Tipo"<<endl;
		cout<<"3. Nombre"<<endl;
	    cout<<"Ingrese filtro de busqueda: ";
	    cin>>a;cout<<endl;
	    switch(a){
	       case 1:cout<<"Ingrese Codigo: ";cin>>filtro;
	            mostrarProductos(filtro);break;
	       case 2:cout<<"Ingrese Tipo de producto: ";cin>>filtro;
	            mostrarProductos(filtro);break;
	       case 3:cout<<"Ingrese Nombre de producto: ";cin>>filtro;
	            mostrarProductos(filtro);break;
	       default: menuBodeguero();
	    }
}

void salirSesion(){
	system("cls");
	cout<<"iniciarSesion()";

}

//##########################################################################################
//##########################################################################################
//##########################################################################################
