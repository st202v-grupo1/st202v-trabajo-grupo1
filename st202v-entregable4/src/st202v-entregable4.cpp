#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

const string NOMBRE_TEMPORAL_COMPRA = "archivotemporalCompra.txt";
const int CANT_REG_CABECERA_TEMP_COMP = 1;
const string NOMBRE_TEMPORAL_VENTA = "archivotemporalVenta.txt";
const string Bodega1 = "archivoVentaBodega1.txt";
const string Bodega2= "archivoVentaBodega2.txt";
const string Bodega3= "archivoVentaBodega3.txt";
const string Cliente1= "archivoCompraCliente1.txt";
const string Cliente2= "archivoCompraCliente2.txt";
const string Cliente3= "archivoCompraCliente3.txt";
const string Cliente4= "archivoCompraCliente4.txt";
const string Cliente5= "archivoCompraCliente5.txt";

//Estructura de un producto
struct producto{
	string codigo;
	string marca;
    string tipo;
	string nombre;
	string unidad;
	string stock;
	string pventa;
	string pcompra;
};
struct operacion{
	int tipo;
	int codigo;
	string fecha;
	string hora;
};
struct filtroVenta{
	int tipo;
	int codigo;
	string fecha;
	string hora;
	string usuario;
};

bool validarCodigo(string cod, string nombreArchivo);
void cargarDatosNuevoProducto(producto p1);
void cargarOperacion(operacion n);
void mostrarRegistro(string f);
void mostrarVenta(string f);
void mostrarProductos(string f);
void RegistrarNuevoProducto();
void consultarRegistro(	);
void consultarVenta();
void consultarProductos();

struct codigo{
	string producto;
	int cantidad;
	int stock;
};
struct pro{
	string codigo;
	string tipo;
	string marca;
	string nombre;
	string unidad;
	string stock;
	string pcompra;
	string pventa;



};
struct Administrador{
	string usuario;
	string nombre;
	string apellido;
	string sexo;
	string correo;
	string telefono;
	string contrasenia;
};
struct Administrador admin;
struct clientes{
	string usuario;
	string nombre;
	string apellido;
	string sexo;
	string correo;
	string direccion;
	string celular;
	string contrasenia;
};
struct clientes usuario;
struct registroCompra {
	string codigo;
	int tipo;
	string nombre;
	string unidad;
	int cantidad;
	int precio;
	int monto;
	string fecha;
	string hora;
	string bodega;
	void inicializar(vector <string> dat) {
		codigo = dat[0];
		tipo = atoi(dat[1].c_str());
		nombre = dat[2];
		unidad = dat[3];
		cantidad = atoi(dat[4].c_str());
		precio = atoi(dat[5].c_str());
		monto = atoi(dat[6].c_str());
		fecha = dat[7];
		hora = dat[8];
		bodega = dat[9];
	}
	void imprimir() {
		cout << " codigo  : " << codigo << endl;
		cout << " tipo  : " << tipo << endl;
		cout << " nombre  : " << nombre << endl;
		cout << " unidad  : " << unidad << endl;
		cout << " cantidad  : " << cantidad << endl;
		cout << " precio  : " << precio << endl;
		cout << " monto  : " << monto << endl;
		cout << " fecha  : " << fecha << endl;
		cout << " hora  : " << hora << endl;
		cout << " bodega  : " << bodega << endl;

	}
}RegistroTmpCompra;

struct bodega{
	string usuario;
	string nombre;
	string apellido;
	string nombreBodega;
	string ruc;
	string sexo;
	string correo;
	string direccion;
	string celular;
	string contrasenia;
};
struct bodega bode;




void consultarCompra();
void mostrarCompras(string filtro);

void crearArchivoTipoProducto(int i);
void solicitarProducto();
void mostrarProductosPorTipo(string filtroS);
void mostrarTipoProducto();
void crearArchivoListaTotalDeProductos();
void crearArchivosDeProductosBodegas();
void mostrarProductoDisponible(string codigo);
void cargarReporteCompraTemp(string user,string bodega,string codigo,string cantidad);
void cargarReporteVentaTemp(string user,string bodega,string codigo,string cantidad);
float convertirAFloat(string cantidad);
int convertirAEntero(string cantidad);

void finalizarCompra();
void cargarTempRealCompra();
//void actualizarStock(string bodega);

void menuBodeguero();
void menuCliente();
void mostrarReporteCompra();
void modoDePago(int modo);
int suma_cif(int y);/////
bool validarTarjeta(long long n);  /////
void borrarTemp(string nombre);
void cargarTempRealVenta();

vector<string> leerLineaArchivoTempComp(string linea, char separador);
struct registroCompra leerDatosRegistroCompra(string linea, char separador);
int preguntaFormaPago();

void salir();
void registrarCliente();
void registrarBodeguero();
void registrarUsuario();
void iniciarSesion();
void MenuDeEntrada();


void menuAdministrador();

void mostrarUsuarios();
void reporteDeUsuarios();

void reportarCompra();
void reporteClientes();

void reportarVenta();
void reportarOperaciones();
void reporteBodeguero();

void buscarFiltro(int filtroI,string filtroS, string nombreArchivo);









int main() {


	MenuDeEntrada();
	return 0;
}
void MenuDeEntrada(){
	cout<<"Bienvenido a EZ Market"<<endl;
	cout<<"(1)Ingresar"<<endl;
	cout<<"(2)Registrar"<<endl;
	cout<<"(3)Salir"<<endl;
	cout <<"Escoger la opcion: ";
	int m;
	cin>>m;
	switch(m){
	case 1:
		iniciarSesion();
		break;
	case 2:
		registrarUsuario();
		break;
	case 3:
		salir();
		break;
	}
}
void iniciarSesion(){
 	vector <struct bodega> b;
 	struct bodega t;
 	string march="registro de bodegueros.txt";
	ifstream archi(march.c_str());
	string linea;
 	if (archi.is_open()){
 		while (!archi.eof()){
 			for(int i=0 ; i<10;i++){
 				char separador;
 				if (i < 9){
 					separador=',';
 				}else{
 					separador='\n';
 				}
 				getline(archi,linea,separador);
 				switch(i){
 					case 0:
 					t.usuario=linea;
 					break;
 					case 1:
 					t.nombre=linea;
 					break;
 					case 2:
 					t.apellido=linea;
 					break;
 					case 3:
 					t.nombreBodega=linea;
 					break;
 					case 4:
 					t.ruc=linea;
 					break;
 					case 5:
 					t.sexo=linea;
 					break;
 					case 6:
 					t.correo=linea;
 					break;
 					case 7:
 					t.direccion=linea;
 					break;
 					case 8:
 					t.celular=linea;
 					break;
 					case 9:
 					t.contrasenia=linea;
 					break;
 				}
 			}
 			b.push_back(t);
 		}
 	}
 	archi.close();
 	vector <struct clientes> c;
 	struct clientes tmp;

	string narch="registro de cliente.txt";
	ifstream arch(narch.c_str());
 	string line;
 	if (arch.is_open()){
 		while (!arch.eof()){
 			for(int i=0 ; i<8;i++){
 				char separador;
 				if (i < 7){
 					separador=',';
 				}else{
 					separador='\n';
 				}
 				getline(arch,line,separador);
 				switch(i){
 					case 0:
 					tmp.usuario=line;
 					break;
 					case 1:
 					tmp.nombre=line;
 					break;
 					case 2:
 					tmp.apellido=line;
 					break;
 					case 3:
 					tmp.sexo=line;
 					break;
 					case 4:
 					tmp.correo=line;
 					break;
 					case 5:
 					tmp.direccion=line;
 					break;
 					case 6:
 					tmp.celular=line;
 					break;
 					case 7:
 					tmp.contrasenia=line;
 					break;
 				}
 			}
 			c.push_back(tmp);
 		}
 	}
 	arch.close();

 	vector <struct Administrador> a;
 	struct Administrador p;

	string parch="registro de administradores.txt";
	ifstream archiv(parch.c_str());
 	string lineas;
 	if (archiv.is_open()){
 		while (!archiv.eof()){
 			for(int i=0 ; i<7;i++){
 				char separador;
 				if (i < 6){
 					separador=',';
 				}else{
 					separador='\n';
 				}
 				getline(archiv,lineas,separador);
 				switch(i){
 					case 0:
 					p.usuario=lineas;
 					break;
 					case 1:
 					p.nombre=lineas;
 					break;
 					case 2:
 					p.apellido=lineas;
 					break;
 					case 3:
 					p.sexo=lineas;
 					break;
 					case 4:
 					p.correo=lineas;
 					break;
 					case 5:
 					p.telefono=lineas;
 					break;
 					case 6:
 					p.contrasenia=lineas;
 					break;
 				}
 			}
 			a.push_back(p);
 		}
 	}else{
 		cout<<"no abrio";
 	}
 	archiv.close();

 	string correo;
 	string contra;
 	string tipo;
 	bool correcto=false;
 	while(!correcto){
 		cout<<endl;
 		cout<<"                  INICIAR SESION                   "<<endl;
 		cout<<"***************************************************"<<endl;
 		cout<< "Correo electronico: ";
 		cin >> correo;
 		cout<< "Contrasenia: ";
 		cin >> contra;
 		cout<<"***************************************************"<<endl;
 		cout<<endl;
 		for(int i=0; i< c.size();i++){
 			if(c[i].correo==correo && c[i].contrasenia==contra){
 				usuario.usuario=c[i].usuario;
 				usuario.nombre=c[i].nombre;
 				usuario.apellido=c[i].apellido;
 				usuario.sexo=c[i].sexo;
 				usuario.correo=c[i].correo;
 				usuario.direccion=c[i].direccion;
 				usuario.celular=c[i].celular;
 				usuario.contrasenia=c[i].contrasenia;
 				tipo="cliente";
 				correcto=true;
 			}
 		}
 		if (!correcto){
 		for(int i=0; i< b.size();i++){
 			if(b[i].correo==correo && b[i].contrasenia==contra){
 				bode.usuario=b[i].usuario;
 				bode.nombre=b[i].nombre;
 				bode.apellido=b[i].apellido;
 				bode.nombreBodega=b[i].nombreBodega;
 				bode.ruc=b[i].ruc;
 				bode.sexo=b[i].sexo;
 				bode.correo=b[i].correo;
 				bode.direccion=b[i].direccion;
 				bode.celular=b[i].celular;
 				bode.contrasenia=b[i].contrasenia;
 				tipo="bodeguero";
 				correcto=true;
 			}
 		}
 		}

 		if (!correcto){
 		for(int i=0; i< a.size();i++){
 			if(a[i].correo==correo && a[i].contrasenia==contra){
 				admin.usuario=a[i].usuario;
 				admin.nombre=a[i].nombre;
 				admin.apellido=a[i].apellido;
 				admin.sexo=a[i].sexo;
 				admin.correo=a[i].correo;
 				admin.telefono=a[i].telefono;
 				admin.contrasenia=a[i].contrasenia;
 				tipo="administrador";
 				correcto=true;
 			}
 		}
 		}
 		if (correcto){
 			cout<<"***** Se ha iniciado correctamente *****"<<endl<< endl;
 		}else{
 			cout << "***** La contrasena o el correo esta mal. *****"<< endl;
 			cout << "***** Vuelve a intentarlo *****"<< endl;
 		}
 	}
 	if (tipo=="cliente"){
 		menuCliente();
 	}
 	if (tipo=="bodeguero"){
 		menuBodeguero();
 	}
 	if (tipo=="administrador"){
 		menuAdministrador();
 	}
}
void registrarUsuario(){
	cout<<"Registro de Usuario"<<endl;
	cout<<"Rol: "<<endl;
	string rol;
	cin>>rol;
	cout<<endl;
	if(rol=="cliente"){
		registrarCliente();
	}
	if(rol=="bodeguero"){
		registrarBodeguero();
	}
}

/*******CLIENTE**********/
void registrarCliente(){
	string t;
	ofstream archivo ;
	string m,n;
	cout<<"\t\tREGISTRO DE CLIENTE"<<endl;
	archivo.open ("registro de cliente.txt",ios::app);
		cout<<"Nombre de Usuario:"<<endl;
		cout<<"\t\t";
		cin>>m;
		getline(cin,n);
		n+=m;
		archivo <<n<<",";
		cout<<"Nombres:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"apellido:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"Sexo:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"Correo Electronico:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"Direccion:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"Numero de Celular:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"Contrasenia:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n;
		archivo<<endl;
		cout<<"�Registrarse?"<<endl;
		cin>>t;
		if (t=="si"){
					menuCliente();
				}
		if(t=="no"){
					registrarUsuario();
				}

	archivo.close();
}
void registrarBodeguero(){
	string t;
	ofstream archivo ;
	string m,n;
	cout<<"\t\tREGISTRO DE BODEGUERO"<<endl;
	archivo.open ("registro de bodegueros.txt",ios::app);
		cout<<"Nombre de Usuario:"<<endl;
		cout<<"\t\t";
		cin>>m;
		getline(cin,n);
		n+=m;
		archivo <<n<<",";
		cout<<"Nombres:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"apellido:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"Nombre de la Bodega:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"RUC:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"Sexo:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"Correo Electronico:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"Direccion:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"Numero de Celular:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n<<",";
		cout<<"Contrasenia:"<<endl;
		cout<<"\t\t";
		getline(cin,n);
		archivo <<n;
		archivo<<endl;
		cout<<"�Registrarse?"<<endl;
		cin>>t;
		if (t=="si"){
			menuBodeguero();
		}
		if(t=="no"){
			registrarUsuario();
		}
	archivo.close();
}
void salir(){
	cout<<"gracias por su visita a EZ MARKET";
}

void menuBodeguero(){
    int a;
	cout<<"Menu del Bodeguero!!!"<<endl;
	cout<<" (1) Registrar Nuevo Producto."<<endl;
	cout<<" (2) Consultar Registro."<<endl;
	cout<<" (3) Consultar Ventas."<<endl;
	cout<<" (4) Consultar Productos."<<endl;
	cout<<" (5) Salir Sesion"<<endl;
	cout<<" Ingrese opcion: ";
	cin>>a;cout<<endl;
	switch(a){
		case 1:RegistrarNuevoProducto();break;
		case 2:	consultarRegistro(); break;
		case 3:	consultarVenta(); break;
		case 4:	consultarProductos(); break;
		case 5:MenuDeEntrada();break;
		default: MenuDeEntrada();
	}
}
void menuCliente(){

	cout<<"OPCIONES"<<endl;
	cout<<"(1) Consulta De Compras"<<endl;
	cout<<"(2) Solicitar Producto"<<endl;
	cout<<"(3) Finalizar Compra"<<endl;
	cout<<"(4) Cerrar Sesion"<<endl;
	int m;
	cout<<"Escoja una opcion ==> ";cin>>m;
	switch(m){
	case 1:
		consultarCompra();break;
	case 2:
		solicitarProducto();break;
	case 3:
		finalizarCompra();break;
	case 4:
		MenuDeEntrada();break;
	default:
		cout<<"opcion no valida"<<endl;
		MenuDeEntrada();
	}

}


void consultarCompra(){
	string filtro;
		int a;
		cout<<"***Consulta de compras***"<<endl;
		cout<<"1. Codigo"<<endl;
		cout<<"2. Fecha"<<endl;
		cout<<"3. Hora"<<endl;
		cout<<"4. Tipo"<<endl;
		cout<<"5. Bodega"<<endl;
		cout<<"6. Volver"<<endl;
	    cout<<"Ingrese filtro de búsqueda: ";
	    cin>>a;cout<<endl;
	    switch(a){
	    case 1:;cout<<"Ingrese Codigo: ";cin>>filtro;
	    	mostrarCompras(filtro);break;
	    case 2:;cout<<"Ingrese Fecha: ";cin>>filtro;
	    mostrarCompras(filtro);break;
	    case 3:;cout<<"Ingrese Hora: ";cin>>filtro;
	    mostrarCompras(filtro);break;
	    case 4:;cout<<"Ingrese Tipo: ";cin>>filtro;
	    mostrarCompras(filtro);break;
	    case 5:;cout<<"Ingrese Bodega: ";cin>>filtro;
	    mostrarCompras(filtro);break;
	    default: ;menuCliente();
	    }
}
void mostrarCompras(string filtro){
	string parch="compra cliente "+usuario.usuario+".txt";
			ifstream arch(parch.c_str());
			string linea,campo1;
					int i;
					int k=1;
					if(arch.is_open()){
						while(!arch.eof()){
						  getline(arch,linea);
						  stringstream s(linea);

							while(!s.eof()){
								for(i=1;i<=11;i++){
									getline(s,campo1,',');
							     	if(campo1==filtro){
							     		cout<<endl;
							     		cout<<"******************************"<<endl;
							     		cout<<"\t\tCOMPRA "<<k<<endl;
							     		k++;
							     		int n=linea.length();
							     		int j=0,cont=0;
							     		while (j<=n){
							     			if (cont ==0){
							     				cout<<"CODIGO:\t";
							     				while (linea[j]!=','){
							     					cout<<linea[j];
							     					j++;
							     				}
							     				cont++;
							     				cout<<endl;
							     				j++;
							     			}
							     			if (cont ==1){
							     				cout<<"TIPO:\t";
							     				while (linea[j]!=','){
							     					cout<<linea[j];
							     					j++;
							     				}
							     				cont++;
							     				cout<<endl;
							     				j++;
							     			}
							     			if (cont ==2){
							     				cout<<"MARCA:\t";
							     				while (linea[j]!=','){
							     					cout<<linea[j];
							     					j++;
							     				}
							     				cont++;
							     				cout<<endl;
							     				j++;
							     			}
							     			if (cont ==3){
							     				cout<<"CLASE:\t";
							     				while (linea[j]!=','){
							     					cout<<linea[j];
							     					j++;
							     				}
							     				cont++;
							     				cout<<endl;
							     				j++;
							     			}
							     			if (cont ==4){
							     				cout<<"CANTIDAD:\t";
							     				while (linea[j]!=','){
							     					cout<<linea[j];
							     					j++;
							     				}
							     				cont++;
							     				cout<<endl;
							     				j++;
							     			}
							     			if (cont ==5){
							     				cout<<"PRECIO DE COMPRA:\t";
							     				while (linea[j]!=','){
							     					cout<<linea[j];
							     					j++;
							     				}
							     				cont++;
							     				cout<<endl;
							     				j++;
							     			}
							     			if (cont ==6){
							     				cout<<"STOCK:\t";
							     				while (linea[j]!=','){
							     					cout<<linea[j];
							     					j++;
							     				}
							     				cont++;
							     				cout<<endl;
							     				j++;
							     			}
							     			if (cont ==7){
							     				cout<<"PRECIO DE VENTA:\t";
							     				while (linea[j]!=','){
							     					cout<<linea[j];
							     					j++;
							     				}
							     				cont++;
							     				cout<<endl;
							     				j++;
							     			}
							     			if (cont ==8){
							     				cout<<"FECHA:\t";
							     				while (linea[j]!=','){
							     					cout<<linea[j];
							     					j++;
							     				}
							     				cont++;
							     				cout<<endl;
							     				j++;
							     			}
							     			if (cont ==9){
							     				cout<<"HORA:\t";
							     				while (linea[j]!=','){
							     					cout<<linea[j];
							     					j++;
							     				}
							     				cont++;
							     				cout<<endl;
							     				j++;
							     			}
							     			if (cont ==10){
							     				cout<<"CODIGO DE BODEGA:\t";
							     				while (linea[j]!='\0'){
							     					cout<<linea[j];
							     					j++;
							     				}
							     				cont++;
							     				cout<<endl;
							     				j++;
							     			}

							     			j++;
							     		}

							     	}

								}


							}

						}

				}
				arch.close();
		  menuCliente();

}

void solicitarProducto(){
	cout<<"Ubicacion actual:"<<endl;
	cout<<"(1) Domicilio"<<endl;
	cout<<"(2) Nueva ubicacion"<<endl;
	int o;cout<<"Escoja opcion ==> ";cin>>o;
	string ub;
	switch(o){
	case 1:
		ub=usuario.direccion;
		break;
	case 2:
		cout<<"Ingrese direccion : ";cin>>ub;break;
	default:
		cout<<"Opcion no valida"<<endl;
		menuCliente();

	}

	cout<<" Buscar por :"<<endl;
	cout<<" (1) Tipo de producto"<<endl;
	cout<<" (2) Codigo del producto"<<endl;
	cout<<" (3) Regresar "<<endl;
	int op;
	cout<<"Escoja opcion ==> ";cin>>op;
	string codi;
	switch (op){
	case 1:
		mostrarTipoProducto();break;
	case 2:
		cout<<"Ingrese el codigo del producto = ";cin>>codi;
		mostrarProductoDisponible(codi);break;
	case 3:
		menuCliente();break;
	default:
		cout<<"opcion no valida"<<endl;
		menuCliente();
	}
}

void crearArchivoTipoProducto(int i){
	string t;
		ofstream archivo ;
		string m,n;
		archivo.open ("tipos de producto.txt",ios::app);
		if(i==1){

			m="PERFUMERIA";
			n+=m;
			archivo <<n<<endl;
			n="LIMPIEZA";
			archivo <<n<<endl;
			n="BEBIDAS-CERVEZAS";
			archivo <<n<<endl;
			n="DESAYUNOS-DULCES";
			archivo <<n<<endl;
			n="QUESOS-FIAMBRES";
			archivo <<n<<endl;
			n="LACTEOS-HUEVOS";
			archivo <<n<<endl;
			n="FRUTAS-VERDURAS";
			archivo <<n<<endl;
			n="ABARROTES";
			archivo <<n;
		}else{
			cout<<"\t\tREGISTRO DE TIPO DE PRODUCTO"<<endl;
					cout<<"Nombre del nuevo tipo de producto:"<<endl;
					cout<<"\t\t";
					cin>>m;
					getline(cin,n);
					n+=m;
					archivo <<n<<endl;
					cout<<"Registrarse? ==> ";cin>>t;
					if (t=="si"){
								menuBodeguero();
							}
					if(t=="no"){
								RegistrarNuevoProducto();
							}
		}
		archivo.close();

}

void crearArchivoListaTotalDeProductos(){
	string t;
		ofstream archivo ;
		string m,n;

		archivo.open ("lista general de productos.txt",ios::app);
		m="PERFUMERIA,000001,DOVE,JABON_EXOFOLIANTE,PACK_3_UN";
			getline(cin,n);
			n+=m;
			archivo <<n<<endl;
			n="PERFUMERIA,000002,COLGATE,TRIPLE_ACCION,PACK_2_UN";
			archivo <<n<<endl;
			n="PERFUMERIA,000003,GILLETTE,PRESTOBARBA_3X2,UNIDAD";
			archivo <<n<<endl;
			n="PERFUMERIA,000004,KOLYNOS,CEPILLO_MASTER_PLUS,UNIDAD";
			archivo <<n<<endl;
			n="PERFUMERIA,000005,SCOTCH_BRITE,PAÑO_MULTIUSOS,PACK_3_UN";
			archivo <<n<<endl;
			n="PERFUMERIA,000006,NIVEA,JABON_CON_AVENA,UNIDAD";
			archivo <<n<<endl;
			n="PERFUMERIA,000007,GILLETE,ESPUMA_DE_AFEITAR,LATA_150_GR";
			archivo <<n<<endl;
			n="PERFUMERIA,000008,COLGATE,CEPILLO_DENTAL,UNIDAD";
			archivo <<n<<endl;
			n="PERFUMERIA,000009,NIVEA,JABON_CON_JOJOBA,UNIDAD";
			archivo <<n<<endl;
			n="PERFUMERIA,000010,DENTO,CREMA_DENTAL,UNIDAD";
			archivo <<n<<endl;

			n="LIMPIEZA,000011,KIWI,BETUN_NEGRO,42_ML";
			archivo <<n<<endl;
			n="LIMPIEZA,000012,ELITE,SERVILLETAS_DOBLADAS,PAQUETE_100_UN";
			archivo <<n<<endl;
			n="LIMPIEZA,000013,SAPOLIO,DESINFECTANTE_LIMPIADOR,PACK_300_ML";
			archivo <<n<<endl;
			n="LIMPIEZA,000014,BOLIVAR,JABON_ANTIBACTERIAL,220_GR";
			archivo <<n<<endl;
			n="LIMPIEZA,000015,SAPOLIO,QUITAMANCHAS_ROPA_COLOR,600_ML";
			archivo <<n<<endl;
			n="LIMPIEZA,000016,AYUDIN,LAVAVAJILLAS,310_GR";
			archivo <<n<<endl;
			n="LIMPIEZA,000017,BAYGON,INSECTICIDA_ESPIRAL,PAQUETE_12_UN";
			archivo <<n<<endl;
			n="LIMPIEZA,000018,MARSELLA,JABON_AROMA_LIMON,220_GR";
			archivo <<n<<endl;
			n="LIMPIEZA,000019,MARSELLA,JABON_AROMA_FLORAL,220_GR";
			archivo <<n<<endl;
			n="LIMPIEZA,000020,ELITE,PAPEL_TOALLA_ULTRA,PAQUETE";
			archivo <<n<<endl;

			n="BEBIDAS-CERVEZAS,000021,COCA_COLA,GASEOSA_NO_RETORNABLE,3_LT";
			archivo <<n<<endl;
			n="BEBIDAS-CERVEZAS,000022,CIFRUT,CITRUS_PUNCH,3_LT";
			archivo <<n<<endl;
			n="BEBIDAS-CERVEZAS,000023,CIELO,AGUA_SIN_GAS,2.5_LT";
			archivo <<n<<endl;
			n="BEBIDAS-CERVEZAS,000024,SAN_MATEO,AGUA_MINERAL_SIN_GAS,2.5_LT";
			archivo <<n<<endl;
			n="BEBIDAS-CERVEZAS,000025,INKA_KOLA,GASEOSAS_ZERO,1.5_LT";
			archivo <<n<<endl;
			n="BEBIDAS-CERVEZAS,000026,PULP,JUGO_SABOR_A_MANGO,1_LT";
			archivo <<n<<endl;
			n="BEBIDAS-CERVEZAS,000027,LAIVE,BEBIDA_SABOR_A_NARANJA,1_LT";
			archivo <<n<<endl;
			n="BEBIDAS-CERVEZAS,000028,CRISTAL,CERVEZA,LATA_355_ML";
			archivo <<n<<endl;
			n="BEBIDAS-CERVEZAS,000029,PILSEN,CERVEZA,LATA_355_ML";
			archivo <<n<<endl;
			n="BEBIDAS-CERVEZAS,000030,CUZQUEÑA,CERVEZA,LATA_355_ML";
			archivo <<n<<endl;

			n="DESAYUNOS-DULCES,000031,PYC,PAN_MOLDE_BLANCO,BOLSA_500_GR";
			archivo <<n<<endl;
			n="DESAYUNOS-DULCES,000032,OREO,GALLETA_DE_CHOCOLATE,UNIDAD";
			archivo <<n<<endl;
			n="DESAYUNOS-DULCES,000033,CASINO,GALLETA_BLACK_FRESA,UNIDAD";
			archivo <<n<<endl;
			n="DESAYUNOS-DULCES,000034,ANGEL,CEREAL_FLAKES,BOLSA_200_GR";
			archivo <<n<<endl;
			n="DESAYUNOS-DULCES,000035,3_OSITOS,QUINUA_AVENA,BOLSA_270_GR";
			archivo <<n<<endl;
			n="DESAYUNOS-DULCES,000036,QUAKER,AVENA_TRADICIONAL,UNIDAD_290_GR";
			archivo <<n<<endl;
			n="DESAYUNOS-DULCES,000037,MC_COLINS,MANZANILLAX25,CAJA";
			archivo <<n<<endl;
			n="DESAYUNOS-DULCES,000038,CAFETAL,CAFE_SELECTO,50_GR";
			archivo <<n<<endl;
			n="DESAYUNOS-DULCES,000038,DULFINA,AZUCAR_RUBIA,BOLSA_1_KG";
			archivo <<n<<endl;
			n="DESAYUNOS-DULCES,000040,FANNY,MERMELADA_FRESA,110_GR";
			archivo <<n<<endl;

			n="QUESOS-FIAMBRES,000041,SAN_FERNANDO,SALCHICHA_DE_POLLO,PAQUETE_250_GR";
			archivo <<n<<endl;
			n="QUESOS-FIAMBRES,000042,LA_SEGOVIANA,JAMON_AMERICANO,180_GR";
			archivo <<n<<endl;
			n="QUESOS-FIAMBRES,000043,BONLE,QUESO_PARMESANO,35_GR";
			archivo <<n<<endl;
			n="QUESOS-FIAMBRES,000044,LAIVE,QUESO_EDAM,180_GR";
			archivo <<n<<endl;
			n="QUESOS-FIAMBRES,000045,SUIZA,HOT_DOG,230_GR";
			archivo <<n<<endl;
			n="QUESOS-FIAMBRES,000046,SAN_FERNANDO,HOT_DOG_DE_PAVITA,250_GR";
			archivo <<n<<endl;
			n="QUESOS-FIAMBRES,000047,SAN_FERNANDO,JAMONADA_DE_POLLO,100_GR";
			archivo <<n<<endl;
			n="QUESOS-FIAMBRES,000048,BONLE,QUESO_EDAM,185_GR";
			archivo <<n<<endl;
			n="QUESOS-FIAMBRES,000049,GLORIA,QUESO_FUNDIDO_UNTABLE,80_GR";
			archivo <<n<<endl;
			n="QUESOS-FIAMBRES,000050,MILKUNZ,QUESO_MOZZARELLA,180_GR";
			archivo <<n<<endl;

			n="LACTEOS-HUEVOS,000051,GLORIA,LECHE_FRESCA_ENTERA,1_LT";
			archivo <<n<<endl;
			n="LACTEOS-HUEVOS,000052,LAIVE,YOGURT_DE_FRESA,946_ML";
			archivo <<n<<endl;
			n="LACTEOS-HUEVOS,000053,LAIVE,YOGURT_BIOMORA,946_ML";
			archivo <<n<<endl;
			n="LACTEOS-HUEVOS,000054,GLORIA,BATTIMIX_C/BOLITA_DE_CHOCOLATE,125_GR";
			archivo <<n<<endl;
			n="LACTEOS-HUEVOS,000055,GLORIA,LECHE_EVAPORADA_LIGTH,400_GR";
			archivo <<n<<endl;
			n="LACTEOS-HUEVOS,000056,LAIVE,MANTEQUILLA_CON_SAL,200_GR";
			archivo <<n<<endl;
			n="LACTEOS-HUEVOS,000057,GLORIA,LECHE_CHOCOLATADA,1_LT";
			archivo <<n<<endl;
			n="LACTEOS-HUEVOS,000058,NESTLE,CREMA_DE_LECHE,300_GR";
			archivo <<n<<endl;
			n="LACTEOS-HUEVOS,000059,LAIVE,BEBIDA_DE_SOYA,1_LT";
			archivo <<n<<endl;
			n="LACTEOS-HUEVOS,000060,VIGOR,LECHE_VIGOR,BOLSA_946_ML";
			archivo <<n<<endl;

			n="FRUTAS-VERDURAS,000061,VERDURAS,CHOCLO,UNIDAD";
			archivo <<n<<endl;
			n="FRUTAS-VERDURAS,000062,VERDURAS,TOMATE,1_KG";
			archivo <<n<<endl;
			n="FRUTAS-VERDURAS,000063,FRUTAS,LIMON,1_KG";
			archivo <<n<<endl;
			n="FRUTAS-VERDURAS,000064,VERDURAS,CEBOLLA_ROJA,1_KG";
			archivo <<n<<endl;
			n="FRUTAS-VERDURAS,000065,VERDURAS,PAPA_AMARILLA,1_KG";
			archivo <<n<<endl;
			n="FRUTAS-VERDURAS,000066,GRANEL,ZANAHORIA,1_KG";
			archivo <<n<<endl;
			n="FRUTAS-VERDURAS,000067,VERDURAS,ZAPALLO,1_KG";
			archivo <<n<<endl;
			n="FRUTAS-VERDURAS,000068,FRUTAS,PLATANO_ISLA,UNIDAD";
			archivo <<n<<endl;
			n="FRUTAS-VERDURAS,000069,VERDURAS,HIERBA_BUENA,UNIDAD";
			archivo <<n<<endl;
			n="FRUTAS-VERDURAS,000070,VERDURAS,CULANTRO,UNIDAD";
			archivo <<n<<endl;
		   n="ABARROTES,000071,PRIMOR,ACEITE_VEGETAL,1_LT";
			archivo <<n<<endl;
			n="ABARROTES,000072,MOLITALIA,FIDEO_SPAGHETTI,1/2_KG";
			archivo <<n<<endl;
			n="ABARROTES,000073,GLORIA,TROZO_DE_ATUN,LATA_170_GR";
			archivo <<n<<endl;
			n="ABARROTES,000074,FANNY,TROZO_DE_ATUN,LATA_170_GR";
			archivo <<n<<endl;
			n="ABARROTES,000075,DOS_CABALLOS,DURAZNO_EN_ALMIBAR,LATA_820_GR";
			archivo <<n<<endl;
			n="ABARROTES,000076,CAMPOMAR,LOMOS_DE_ATUN_EN_ACEITE,LATA_170_GR";
			archivo <<n<<endl;
			n="ABARROTES,000077,ALACENA,MAYONESA,200_GR";
			archivo <<n<<endl;
			n="ABARROTES,000078,AJINOMOTO,DOÑA_GUSTA,1_SOBRE";
			archivo <<n<<endl;
			n="ABARROTES,000079,MOLITALIA,SALSA_TOMATE_POMAROLA,320_GR";
			archivo <<n<<endl;
			n="ABARROTES,000080,EMSAL,SAL_DE_COCINA,500_GR";
			archivo <<n<<endl;

		archivo.close();

}
void mostrarProductosPorTipo(string filtroS){
	string nombreArchivo1="lista general de productos.txt";

	    ifstream archivo1(nombreArchivo1.c_str());
		string linea,campo1;
		int i;
		if(archivo1.is_open()){
			while(!archivo1.eof()){
			  getline(archivo1,linea);
			  stringstream s(linea);
				while(!s.eof()){
					getline(s,campo1,',');
					if(campo1==filtroS){
						for(i=2;i<=5;i++){
							getline(s,campo1,',');
							cout<<campo1<<" ";
								}cout<<endl;
						}

					     }


					}

			}
					archivo1.close();

}

void mostrarTipoProducto(){
	string nombreArchivo = "tipos de producto.txt";
	ifstream archivo(nombreArchivo.c_str());
	if(archivo.is_open()){
		string linea;
		int i=1;
		while(!archivo.eof()){
		getline(archivo,linea);
		cout<<"("<<i<<")"<<linea<<endl;
		i++;
		}
	archivo.close();
	cout<<"("<<i<<")Regresar"<<endl;

	int opcion;
	cout<<"Escoja opcion ==> ";cin>>opcion;

	cout<<"CODIGO     MARCA    DESCRIPCION    UNIDAD"<<endl;
	string cod;string resp,filtro;
	switch (opcion){
	case 1:
        filtro="PERFUMERIA";
		mostrarProductosPorTipo(filtro);
		cout<<endl;
		cout<<"Ingrese el codigo del producto ==> ";cin>>cod;
		mostrarProductoDisponible(cod);
		cout<<"Desea solicitar otro producto? (S/N) ==> ";cin>>resp;
		if(resp=="S" || resp=="s"){
			solicitarProducto();
		}else{
			menuCliente();
		}

		;break;
	case 2:
		filtro="LIMPIEZA";
		mostrarProductosPorTipo(filtro);

		cout<<"Ingrese el codigo del producto ==> ";cin>>cod;
		mostrarProductoDisponible(cod);
		cout<<"Desea solicitar otro producto? (S/N) ==> ";cin>>resp;
				if(resp=="S" || resp=="s"){
					solicitarProducto();
				}else{
					menuCliente();
				}
		;
		break;
	case 3:
		filtro="BEBIDAS-CERVEZAS";
		mostrarProductosPorTipo(filtro);
	    cout<<"Ingrese el codigo del producto ==> ";cin>>cod;
		mostrarProductoDisponible(cod);
		cout<<"Desea solicitar otro producto? (S/N) ==> ";cin>>resp;
				if(resp=="S" || resp=="s"){
					solicitarProducto();
				}else{
					menuCliente();
				}
		;break;
	case 4:
		filtro="DESAYUNOS-DULCES";
		mostrarProductosPorTipo(filtro);
	    cout<<"Ingrese el codigo del producto ==> ";cin>>cod;
		mostrarProductoDisponible(cod);
		cout<<"Desea solicitar otro producto? (S/N) ==> ";cin>>resp;
				if(resp=="S" || resp=="s"){
					solicitarProducto();
				}else{
					menuCliente();
				}
		;
		break;
	case 5:
		filtro="QUESOS-FIAMBRES";
		mostrarProductosPorTipo(filtro);
	    cout<<"Ingrese el codigo del producto ==> ";cin>>cod;
		mostrarProductoDisponible(cod);
		cout<<"Desea solicitar otro producto? (S/N) ==> ";cin>>resp;
				if(resp=="S" || resp=="s"){
					solicitarProducto();
				}else{
					menuCliente();
				}
		;break;
	case 6:
		filtro="LACTEOS-HUEVOS";
		mostrarProductosPorTipo(filtro);
		    cout<<"Ingrese el codigo del producto ==> ";cin>>cod;
			mostrarProductoDisponible(cod);
			cout<<"Desea solicitar otro producto? (S/N) ==> ";cin>>resp;
					if(resp=="S" || resp=="s"){
						solicitarProducto();
					}else{
						menuCliente();
					}
			;break;
	case 7:
		filtro="FRUTAS-VERDURAS";
		mostrarProductosPorTipo(filtro);
		    cout<<"Ingrese el codigo del producto ==> ";cin>>cod;
			mostrarProductoDisponible(cod);
			cout<<"Desea solicitar otro producto? (S/N) ==> ";cin>>resp;
					if(resp=="S" || resp=="s"){
						solicitarProducto();
					}else{
						menuCliente();
					}
			;break;
	case 8:
		filtro="ABARROTES";
		mostrarProductosPorTipo(filtro);
			cout<<"Ingrese el codigo del producto ==> ";cin>>cod;
			mostrarProductoDisponible(cod);
			cout<<"Desea solicitar otro producto? (S/N) ==> ";cin>>resp;
					if(resp=="S" || resp=="s"){
						solicitarProducto();
					}else{
						menuCliente();
					}
			;break;
	case 9: menuCliente();break;
	default:
		cout<<"Opcion no valida"<<endl;
		menuCliente();
	}

	}else{
	cout<<"No se pudo abrir archivo";
	menuCliente();
	}
}

void mostrarProductoDisponible(string codigo){

	string nombreArch="lista de productos bodega 1000.txt";
	string campo1,linea;
	int i;
	ifstream archivo2(nombreArch.c_str());

	if(archivo2.is_open()){
		while(!archivo2.eof()){
			getline(archivo2,linea);
			stringstream a(linea);
			while(!a.eof()){
				getline(a,campo1,',');
				if(campo1==codigo){
						cout<<"  **********************BODEGA TIO BENITO - 1000**************"<<endl;
						cout<<"Codigo            Marca              Descripcion               Stock        Precio  "<<endl;
						for(i=2;i<=8;i++){
							getline(a,campo1,',');
							if(i==3){cout<<codigo<<"            "<<campo1<<"         ";
							}
							if(i==4){cout<<campo1<<"         ";
							}
							if(i==6){cout<<campo1<<"           ";
																									}
							if(i==8){cout<<campo1<<"         ";
							}

						}cout<<endl;
				}
			}
		}
	}
	archivo2.close();

	string nombreArch2="lista de productos bodega 2000.txt";
	string campo2;
	ifstream archivo3(nombreArch2.c_str());
	if(archivo3.is_open()){
		while(!archivo3.eof()){
			getline(archivo3,linea);
			stringstream a(linea);
			while(!a.eof()){
				getline(a,campo2,',');
				if(campo2==codigo){
					cout<<"  **********************BODEGA DOÑA MECHE - 2000**************"<<endl;
					cout<<"Codigo            Marca              Descripcion               Stock        Precio  "<<endl;
					for(i=2;i<=8;i++){
						getline(a,campo2,',');
						if(i==3){cout<<codigo<<"            "<<campo2<<"         ";
     					}
						if(i==4){cout<<campo2<<"         ";
						}
						if(i==6){cout<<campo2<<"           ";
																		}
						if(i==8){cout<<campo2<<"         ";
						}
					}cout<<endl;
				}
			}
		}
	}
	archivo3.close();

	string nombreArch3="lista de productos bodega 3000.txt";
	string campo3;
	ifstream archivo4(nombreArch3.c_str());
	if(archivo4.is_open()){
		while(!archivo4.eof()){
			getline(archivo4,linea);
			stringstream a(linea);
			while(!a.eof()){
				getline(a,campo3,',');
				if(campo3==codigo){
					cout<<"  **********************BODEGA TAMBO - 3000**************"<<endl;
					cout<<"Codigo            Marca              Descripcion               Stock        Precio  "<<endl;
					for(i=2;i<=8;i++){
						getline(a,campo3,',');
						if(i==3){cout<<codigo<<"            "<<campo3<<"         ";
						}
						if(i==4){cout<<campo3<<"         ";
						}
						if(i==6){cout<<campo3<<"           ";
												}
						if(i==8){cout<<campo3<<"         ";
						}
					}cout<<endl;
				}
			}
		}
	}
	archivo4.close();

	string codbo;
	cout<<"Ingrese el codigo de la bodega ==> ";cin>>codbo;cout<<endl;
	string cant;
	cout<<"Ingrese la cantidad que desea comprar ==> ";cin>>cant;cout<<endl;
	cargarReporteCompraTemp(usuario.usuario,codbo,codigo,cant);
	cargarReporteVentaTemp(usuario.usuario,codbo,codigo,cant);
}



void crearArchivosDeProductosBodegas(){
	string t;
			ofstream archivo,archivo2,archivo3 ;
			string m,n;
/*
			archivo.open ("lista de productos bodega 1000.txt",ios::app);
			m="000001,PERFUMERIA,DOVE,JABON_EXOFOLIANTE,PACK_3_UN,40,6.5,7.2";
				getline(cin,n);
				n+=m;
				archivo <<n<<endl;
				n="000002,PERFUMERIA,COLGATE,TRIPLE_ACCION,PACK_2_UN,50,7,7.7";
				archivo <<n<<endl;
				n="000003,PERFUMERIA,GILLETTE,PRESTOBARBA_3X2,UNIDAD,30,7.2,8";
				archivo <<n<<endl;
				n="000004,PERFUMERIA,KOLYNOS,CEPILLO_MASTER_PLUS,UNIDAD,20,2,2.5";
				archivo <<n<<endl;
				n="000005,PERFUMERIA,SCOTCH_BRITE,PAÑO_MULTIUSOS,PACK_3_UN,20,4,4.2";
				archivo <<n<<endl;
				n="000006,PERFUMERIA,NIVEA,JABON_CON_AVENA,UNIDAD,30,2.5,3";
				archivo <<n<<endl;
				n="000007,PERFUMERIA,GILLETE,ESPUMA_DE_AFEITAR,LATA_150_GR,10,12,13";
				archivo <<n<<endl;
				n="000008,PERFUMERIA,COLGATE,CEPILLO_DENTAL,UNIDAD,40,2.8,3.5";
				archivo <<n<<endl;
				n="000009,PERFUMERIA,NIVEA,JABON_CON_JOJOBA,UNIDAD,30,1.5,2";
				archivo <<n<<endl;
				n="000010,PERFUMERIA,DENTO,CREMA_DENTAL,UNIDAD,15,2.4,3";
				archivo <<n<<endl;

				n="000011,LIMPIEZA,KIWI,BETUN_NEGRO,42_ML,20,2,2.3";
				archivo <<n<<endl;
				n="000012,LIMPIEZA,ELITE,SERVILLETAS_DOBLADAS,PAQUETE_100_UN,25,2,2.2";
				archivo <<n<<endl;
				n="000013,LIMPIEZA,SAPOLIO,DESINFECTANTE_LIMPIADOR,PACK_300_ML,15,2,2.4";
				archivo <<n<<endl;
				n="000014,LIMPIEZA,BOLIVAR,JABON_ANTIBACTERIAL,220_GR,15,1.5,2";
				archivo <<n<<endl;
				n="000015,LIMPIEZA,SAPOLIO,QUITAMANCHAS_ROPA_COLOR,600_ML,10,2.5,3";
				archivo <<n<<endl;
				n="000016,LIMPIEZA,AYUDIN,LAVAVAJILLAS,310_GR,30,2.6,3";
				archivo <<n<<endl;
				n="000017,LIMPIEZA,BAYGON,INSECTICIDA_ESPIRAL,PAQUETE_12_UN,15,2.6,3";
				archivo <<n<<endl;
				n="000018,LIMPIEZA,MARSELLA,JABON_AROMA_LIMON,220_GR,10,1.6,2";
				archivo <<n<<endl;
				n="000019,LIMPIEZA,MARSELLA,JABON_AROMA_FLORAL,220_GR,10,1.6,2";
				archivo <<n<<endl;
				n="000020,LIMPIEZA,ELITE,PAPEL_TOALLA_ULTRA,PAQUETE,20,2.5,3";
				archivo <<n<<endl;

				n="000021,BEBIDAS-CERVEZAS,COCA_COLA,GASEOSA_NO_RETORNABLE,3_LT,20,6,7";
				archivo <<n<<endl;
				n="000022,BEBIDAS-CERVEZAS,CIFRUT,CITRUS_PUNCH,3_LT,20,2.8,3";
				archivo <<n<<endl;
				n="000023,BEBIDAS-CERVEZAS,CIELO,AGUA_SIN_GAS,2.5_LT,30,1.5,2";
				archivo <<n<<endl;
				n="000024,BEBIDAS-CERVEZAS,SAN_MATEO,AGUA_MINERAL_SIN_GAS,2.5_LT,25,2.8,3.2";
				archivo <<n<<endl;
				n="000025,BEBIDAS-CERVEZAS,INKA_KOLA,GASEOSAS_ZERO,1.5_LT,24,4,4.5";
				archivo <<n<<endl;
				n="000026,BEBIDAS-CERVEZAS,PULP,JUGO_SABOR_A_MANGO,1_LT,10,1.8,2.5";
				archivo <<n<<endl;
				n="000027,BEBIDAS-CERVEZAS,LAIVE,BEBIDA_SABOR_A_NARANJA,1_LT,10,1.8,2.5";
				archivo <<n<<endl;
				n="000028,BEBIDAS-CERVEZAS,CRISTAL,CERVEZA,LATA_355_ML,24,4.5,5";
				archivo <<n<<endl;
				n="000029,BEBIDAS-CERVEZAS,PILSEN,CERVEZA,LATA_355_ML,24,5,5.5";
				archivo <<n<<endl;
				n="000030,BEBIDAS-CERVEZAS,CUZQUEÑA,CERVEZA,LATA_355_ML,24,3.2,3.8";
				archivo <<n<<endl;

				n="000031,DESAYUNOS-DULCES,PYC,PAN_MOLDE_BLANCO,BOLSA_500_GR,15,5.5,6";
				archivo <<n<<endl;
				n="000032,DESAYUNOS-DULCES,OREO,GALLETA_DE_CHOCOLATE,UNIDAD,24,0.5,1";
				archivo <<n<<endl;
				n="000033,DESAYUNOS-DULCES,CASINO,GALLETA_BLACK_FRESA,UNIDAD,24,0.8,1";
				archivo <<n<<endl;
				n="000034,DESAYUNOS-DULCES,ANGEL,CEREAL_FLAKES,BOLSA_200_GR,15,5,6.5";
				archivo <<n<<endl;
				n="000035,DESAYUNOS-DULCES,3_OSITOS,QUINUA_AVENA,BOLSA_270_GR,15,3,4";
				archivo <<n<<endl;
				n="000036,DESAYUNOS-DULCES,QUAKER,AVENA_TRADICIONAL,UNIDAD_290_GR,20,4.2,5";
				archivo <<n<<endl;
				n="000037,DESAYUNOS-DULCES,MC_COLINS,MANZANILLAX25,CAJA,20,2,2.5";
				archivo <<n<<endl;
				n="000038,DESAYUNOS-DULCES,CAFETAL,CAFE_SELECTO,50_GR,30,1.6,2";
				archivo <<n<<endl;
				n="000038,DESAYUNOS-DULCES,DULFINA,AZUCAR_RUBIA,BOLSA_1_KG,20,3,3.5";
				archivo <<n<<endl;
				n="000040,DESAYUNOS-DULCES,FANNY,MERMELADA_FRESA,110_GR,30,1,1.5";
				archivo <<n<<endl;

				n="000041,QUESOS-FIAMBRES,SAN_FERNANDO,SALCHICHA_DE_POLLO,PAQUETE_250_GR,20,3.5,4";
				archivo <<n<<endl;
				n="000042,QUESOS-FIAMBRES,LA_SEGOVIANA,JAMON_AMERICANO,180_GR,15,4.5,5";
				archivo <<n<<endl;
				n="000043,QUESOS-FIAMBRES,BONLE,QUESO_PARMESANO,35_GR,20,3.5,4";
				archivo <<n<<endl;
				n="000044,QUESOS-FIAMBRES,LAIVE,QUESO_EDAM,180_GR,20,8,9";
				archivo <<n<<endl;
				n="000045,QUESOS-FIAMBRES,SUIZA,HOT_DOG,230_GR,15,3.5,4";
				archivo <<n<<endl;
				n="000046,QUESOS-FIAMBRES,SAN_FERNANDO,HOT_DOG_DE_PAVITA,250_GR,15,5.5,6.5";
				archivo <<n<<endl;
				n="000047,QUESOS-FIAMBRES,SAN_FERNANDO,JAMONADA_DE_POLLO,100_GR,20,2,2.5";
				archivo <<n<<endl;
				n="000048,QUESOS-FIAMBRES,BONLE,QUESO_EDAM,185_GR,15,7,8";
				archivo <<n<<endl;
				n="000049,QUESOS-FIAMBRES,GLORIA,QUESO_FUNDIDO_UNTABLE,80_GR,20,2,2.8";
				archivo <<n<<endl;
				n="000050,QUESOS-FIAMBRES,MILKUNZ,QUESO_MOZZARELLA,180_GR,20,7.5,8";
				archivo <<n<<endl;
				archivo.close();

			archivo2.open ("lista de productos bodega 2000.txt",ios::app);

	  		n="000051,LACTEOS-HUEVOS,GLORIA,LECHE_FRESCA_ENTERA,1_LT,15,3.8,4.5";
				archivo2 <<n<<endl;
				n="000052,LACTEOS-HUEVOS,LAIVE,YOGURT_DE_FRESA,946_ML,15,5,5.5";
				archivo2 <<n<<endl;
				n="000053,LACTEOS-HUEVOS,LAIVE,YOGURT_BIOMORA,946_ML,15,5,5.5";
				archivo2 <<n<<endl;
				n="000054,LACTEOS-HUEVOS,GLORIA,BATTIMIX_C/BOLITA_DE_CHOCOLATE,125_GR,20,2.5,3.5";
				archivo2 <<n<<endl;
				n="000055,LACTEOS-HUEVOS,GLORIA,LECHE_EVAPORADA_LIGTH,400_GR,30,3,3.5";
				archivo2 <<n<<endl;
				n="000056,LACTEOS-HUEVOS,LAIVE,MANTEQUILLA_CON_SAL,200_GR,20,7,8.7";
				archivo2 <<n<<endl;
				n="000057,LACTEOS-HUEVOS,GLORIA,LECHE_CHOCOLATADA,1_LT,20,4,4.5";
				archivo2 <<n<<endl;
				n="000058,LACTEOS-HUEVOS,NESTLE,CREMA_DE_LECHE,300_GR,15,7.5,8";
				archivo2 <<n<<endl;
				n="000059,LACTEOS-HUEVOS,LAIVE,BEBIDA_DE_SOYA,1_LT,12,4,5";
				archivo2 <<n<<endl;
				n="000060,LACTEOS-HUEVOS,VIGOR,LECHE_VIGOR,BOLSA_946_ML,24,3.5,4.2";
				archivo2 <<n<<endl;

				n="000021,BEBIDAS-CERVEZAS,COCA_COLA,GASEOSA_NO_RETORNABLE,3_LT,20,6,7.1";
				archivo2 <<n<<endl;
				n="000022,BEBIDAS-CERVEZAS,CIFRUT,CITRUS_PUNCH,3_LT,20,2.8,3.2";
				archivo2 <<n<<endl;
				n="000023,BEBIDAS-CERVEZAS,CIELO,AGUA_SIN_GAS,2.5_LT,30,1.5,2.1";
				archivo2 <<n<<endl;
				n="000024,BEBIDAS-CERVEZAS,SAN_MATEO,AGUA_MINERAL_SIN_GAS,2.5_LT,25,2.8,3.2";
				archivo2 <<n<<endl;
				n="000025,BEBIDAS-CERVEZAS,INKA_KOLA,GASEOSAS_ZERO,1.5_LT,24,4,4.5";
				archivo2 <<n<<endl;
				n="000026,BEBIDAS-CERVEZAS,PULP,JUGO_SABOR_A_MANGO,1_LT,10,1.8,2.5";
				archivo2 <<n<<endl;
				n="000027,BEBIDAS-CERVEZAS,LAIVE,BEBIDA_SABOR_A_NARANJA,1_LT,10,1.8,2.5";
				archivo2 <<n<<endl;
				n="000028,BEBIDAS-CERVEZAS,CRISTAL,CERVEZA,LATA_355_ML,24,4.5,5.5";
				archivo2 <<n<<endl;
				n="000029,BEBIDAS-CERVEZAS,PILSEN,CERVEZA,LATA_355_ML,24,5,6";
				archivo2 <<n<<endl;
				n="000030,BEBIDAS-CERVEZAS,CUZQUEÑA,CERVEZA,LATA_355_ML,24,3.2,3.8";
				archivo2 <<n<<endl;

				n="000061,FRUTAS-VERDURAS,VERDURAS,CHOCLO,UNIDAD,10,0.7,1.2";
				archivo2 <<n<<endl;
				n="000062,FRUTAS-VERDURAS,VERDURAS,TOMATE,1_KG,8,3.8,4.5";
				archivo2 <<n<<endl;
				n="000063,FRUTAS-VERDURAS,FRUTAS,LIMON,1_KG,12,4.8,5.5";
				archivo2 <<n<<endl;
				n="000064,FRUTAS-VERDURAS,VERDURAS,CEBOLLA_ROJA,1_KG,10,1.5,1.8";
				archivo2 <<n<<endl;
				n="000065,FRUTAS-VERDURAS,VERDURAS,PAPA_AMARILLA,1_KG,15,3,3.5";
				archivo2 <<n<<endl;
				n="000066,FRUTAS-VERDURAS,GRANEL,ZANAHORIA,1_KG,10,1,1.5";
				archivo2 <<n<<endl;
				n="000067,FRUTAS-VERDURAS,VERDURAS,ZAPALLO,1_KG,5,1.8,2.5";
				archivo2 <<n<<endl;
				n="000068,FRUTAS-VERDURAS,FRUTAS,PLATANO_ISLA,UNIDAD,20,0.4,0.7";
				archivo2 <<n<<endl;
				n="000069,FRUTAS-VERDURAS,VERDURAS,HIERBA_BUENA,UNIDAD,15,1,1.5";
				archivo2 <<n<<endl;
				n="000070,FRUTAS-VERDURAS,VERDURAS,CULANTRO,UNIDAD,15,0.8,1.1";
				archivo2 <<n<<endl;

				n="000031,DESAYUNOS-DULCES,PYC,PAN_MOLDE_BLANCO,BOLSA_500_GR,15,5.5,6.1";
				archivo2 <<n<<endl;
				n="000032,DESAYUNOS-DULCES,OREO,GALLETA_DE_CHOCOLATE,UNIDAD,24,0.5,1.2";
				archivo2 <<n<<endl;
				n="000033,DESAYUNOS-DULCES,CASINO,GALLETA_BLACK_FRESA,UNIDAD,24,0.8,1";
				archivo2 <<n<<endl;
				n="000034,DESAYUNOS-DULCES,ANGEL,CEREAL_FLAKES,BOLSA_200_GR,15,5,6.5";
				archivo2 <<n<<endl;
				n="000035,DESAYUNOS-DULCES,3_OSITOS,QUINUA_AVENA,BOLSA_270_GR,15,3,4";
				archivo2 <<n<<endl;
				n="000036,DESAYUNOS-DULCES,QUAKER,AVENA_TRADICIONAL,UNIDAD_290_GR,20,4.3";
				archivo2 <<n<<endl;
				n="000037,DESAYUNOS-DULCES,MC_COLINS,MANZANILLAX25,CAJA,20,2,3";
				archivo2 <<n<<endl;
				n="000038,DESAYUNOS-DULCES,CAFETAL,CAFE_SELECTO,50_GR,30,1.6,2";
				archivo2 <<n<<endl;
				n="000038,DESAYUNOS-DULCES,DULFINA,AZUCAR_RUBIA,BOLSA_1_KG,20,3,3.5";
				archivo2 <<n<<endl;
				n="000040,DESAYUNOS-DULCES,FANNY,MERMELADA_FRESA,110_GR,30,1,1.5";
				archivo2 <<n<<endl;

			   n="000071,ABARROTES,PRIMOR,ACEITE_VEGETAL,1_LT,24,6.2,7";
				archivo2 <<n<<endl;
				n="000072,ABARROTES,MOLITALIA,FIDEO_SPAGHETTI,1/2_KG,24,1.8,2.2";
				archivo2 <<n<<endl;
				n="000073,ABARROTES,GLORIA,TROZO_DE_ATUN,LATA_170_GR,20,6.2,7";
				archivo2 <<n<<endl;
				n="000074,ABARROTES,FANNY,TROZO_DE_ATUN,LATA_170_GR,15,4.6,5";
				archivo2 <<n<<endl;
				n="000075,ABARROTES,DOS_CABALLOS,DURAZNO_EN_ALMIBAR,LATA_820_GR,20,6.5,7.5";
				archivo2 <<n<<endl;
				n="000076,ABARROTES,CAMPOMAR,LOMOS_DE_ATUN_EN_ACEITE,LATA_170_GR,15,4.8,5.5";
				archivo2 <<n<<endl;
				n="000077,ABARROTES,ALACENA,MAYONESA,200_GR,15,2.8,3.8";
				archivo2 <<n<<endl;
				n="000078,ABARROTES,AJINOMOTO,DOÑA_GUSTA,1_SOBRE,24,0.2,0.3";
				archivo2 <<n<<endl;
				n="000079,ABARROTES,MOLITALIA,SALSA_TOMATE_POMAROLA,320_GR,20,1.5,2";
				archivo2 <<n<<endl;
				n="000080,ABARROTES,EMSAL,SAL_DE_COCINA,500_GR,20,0.7,0.9";
				archivo2 <<n<<endl;
		       archivo2.close();

      		archivo3.open ("lista de productos bodega 3000.txt",ios::app);
	     	n="000041,QUESOS-FIAMBRES,SAN_FERNANDO,SALCHICHA_DE_POLLO,PAQUETE_250_GR,20,3.5,4";
			archivo3 <<n<<endl;
			n="000042,QUESOS-FIAMBRES,LA_SEGOVIANA,JAMON_AMERICANO,180_GR,15,4.5,5";
			archivo3 <<n<<endl;
			n="000043,QUESOS-FIAMBRES,BONLE,QUESO_PARMESANO,35_GR,20,3.5,4";
			archivo3 <<n<<endl;
			n="000044,QUESOS-FIAMBRES,LAIVE,QUESO_EDAM,180_GR,20,8,9.2";
			archivo3 <<n<<endl;
			n="000045,QUESOS-FIAMBRES,SUIZA,HOT_DOG,230_GR,15,3.5,4";
			archivo3 <<n<<endl;
			n="000046,QUESOS-FIAMBRES,SAN_FERNANDO,HOT_DOG_DE_PAVITA,250_GR,15,5.5,6";
			archivo3 <<n<<endl;
			n="000047,QUESOS-FIAMBRES,SAN_FERNANDO,JAMONADA_DE_POLLO,100_GR,20,2,2.5";
			archivo3 <<n<<endl;
			n="000048,QUESOS-FIAMBRES,BONLE,QUESO_EDAM,185_GR,15,7,8";
			archivo3 <<n<<endl;
			n="000049,QUESOS-FIAMBRES,GLORIA,QUESO_FUNDIDO_UNTABLE,80_GR,20,2,2.8";
			archivo3 <<n<<endl;
			n="000050,QUESOS-FIAMBRES,MILKUNZ,QUESO_MOZZARELLA,180_GR,20,7.5,8.2";
			archivo3 <<n<<endl;

			n="000011,LIMPIEZA,KIWI,BETUN_NEGRO,42_ML,20,2,2.3";
			archivo3 <<n<<endl;
			n="000012,LIMPIEZA,ELITE,SERVILLETAS_DOBLADAS,PAQUETE_100_UN,25,2,2.2";
			archivo3 <<n<<endl;
			n="000013,LIMPIEZA,SAPOLIO,DESINFECTANTE_LIMPIADOR,PACK_300_ML,15,2,2.5";
			archivo3 <<n<<endl;
			n="000014,LIMPIEZA,BOLIVAR,JABON_ANTIBACTERIAL,220_GR,15,1.5,2.2";
			archivo3 <<n<<endl;
			n="000015,LIMPIEZA,SAPOLIO,QUITAMANCHAS_ROPA_COLOR,600_ML,10,2.5,3";
			archivo3 <<n<<endl;
			n="000016,LIMPIEZA,AYUDIN,LAVAVAJILLAS,310_GR,30,2.6,3";
			archivo3 <<n<<endl;
			n="000017,LIMPIEZA,BAYGON,INSECTICIDA_ESPIRAL,PAQUETE_12_UN,15,2.6,3.5";
			archivo3 <<n<<endl;
			n="000018,LIMPIEZA,MARSELLA,JABON_AROMA_LIMON,220_GR,10,1.6,2";
			archivo3 <<n<<endl;
			n="000019,LIMPIEZA,MARSELLA,JABON_AROMA_FLORAL,220_GR,10,1.6,2";
			archivo3 <<n<<endl;
			n="000020,LIMPIEZA,ELITE,PAPEL_TOALLA_ULTRA,PAQUETE,20,2.5,3";
			archivo3 <<n<<endl;

			n="000061,FRUTAS-VERDURAS,VERDURAS,CHOCLO,UNIDAD,10,0.7,1.2";
			archivo3 <<n<<endl;
			n="000062,FRUTAS-VERDURAS,VERDURAS,TOMATE,1_KG,8,3.8,4.5";
			archivo3 <<n<<endl;
			n="000063,FRUTAS-VERDURAS,FRUTAS,LIMON,1_KG,12,4.8,5.5";
			archivo3 <<n<<endl;
			n="000064,FRUTAS-VERDURAS,VERDURAS,CEBOLLA_ROJA,1_KG,10,1.5,2";
			archivo3 <<n<<endl;
			n="000065,FRUTAS-VERDURAS,VERDURAS,PAPA_AMARILLA,1_KG,15,3,3.5";
			archivo3 <<n<<endl;
			n="000066,FRUTAS-VERDURAS,GRANEL,ZANAHORIA,1_KG,10,1,2";
			archivo3 <<n<<endl;
			n="000067,FRUTAS-VERDURAS,VERDURAS,ZAPALLO,1_KG,5,1.8,2.5";
			archivo3 <<n<<endl;
			n="000068,FRUTAS-VERDURAS,FRUTAS,PLATANO_ISLA,UNIDAD,20,0.4,1";
			archivo3 <<n<<endl;
			n="000069,FRUTAS-VERDURAS,VERDURAS,HIERBA_BUENA,UNIDAD,15,1,1.5";
			archivo3 <<n<<endl;
			n="000070,FRUTAS-VERDURAS,VERDURAS,CULANTRO,UNIDAD,15,0.8,1.1";
			archivo3 <<n<<endl;

			m="000001,PERFUMERIA,DOVE,JABON_EXOFOLIANTE,PACK_3_UN,40,6.5,7.2";
			archivo3 <<m<<endl;
			n="000002,PERFUMERIA,COLGATE,TRIPLE_ACCION,PACK_2_UN,50,7,7.7";
			archivo3 <<n<<endl;
			n="000003,PERFUMERIA,GILLETTE,PRESTOBARBA_3X2,UNIDAD,30,7.2,8";
			archivo3 <<n<<endl;
			n="000004,PERFUMERIA,KOLYNOS,CEPILLO_MASTER_PLUS,UNIDAD,20,2,2.5";
			archivo3 <<n<<endl;
			n="000005,PERFUMERIA,SCOTCH_BRITE,PAÑO_MULTIUSOS,PACK_3_UN,20,4,4.2";
			archivo3 <<n<<endl;
			n="000006,PERFUMERIA,NIVEA,JABON_CON_AVENA,UNIDAD,30,2.5,3.5";
			archivo3 <<n<<endl;
			n="000007,PERFUMERIA,GILLETE,ESPUMA_DE_AFEITAR,LATA_150_GR,10,12,13";
			archivo3 <<n<<endl;
			n="000008,PERFUMERIA,COLGATE,CEPILLO_DENTAL,UNIDAD,40,2.8,3";
			archivo3 <<n<<endl;
			n="000009,PERFUMERIA,NIVEA,JABON_CON_JOJOBA,UNIDAD,30,1.5,2";
			archivo3 <<n<<endl;
			n="000010,PERFUMERIA,DENTO,CREMA_DENTAL,UNIDAD,15,2.4,3";
			archivo3 <<n<<endl;

			n="000071,ABARROTES,PRIMOR,ACEITE_VEGETAL,1_LT,24,6.2,7";
			archivo3 <<n<<endl;
			n="000072,ABARROTES,MOLITALIA,FIDEO_SPAGHETTI,1/2_KG,24,1.8,2.2";
			archivo3 <<n<<endl;
			n="000073,ABARROTES,GLORIA,TROZO_DE_ATUN,LATA_170_GR,20,6.2,7";
			archivo3 <<n<<endl;
			n="000074,ABARROTES,FANNY,TROZO_DE_ATUN,LATA_170_GR,15,4.6,5";
			archivo3 <<n<<endl;
			n="000075,ABARROTES,DOS_CABALLOS,DURAZNO_EN_ALMIBAR,LATA_820_GR,20,6.5,7.5";
			archivo3 <<n<<endl;
			n="000076,ABARROTES,CAMPOMAR,LOMOS_DE_ATUN_EN_ACEITE,LATA_170_GR,15,4.8,5.5";
			archivo3 <<n<<endl;
			n="000077,ABARROTES,ALACENA,MAYONESA,200_GR,15,2.8,4";
			archivo3 <<n<<endl;
			n="000078,ABARROTES,AJINOMOTO,DOÑA_GUSTA,1_SOBRE,24,0.2,0.3";
			archivo3 <<n<<endl;
			n="000079,ABARROTES,MOLITALIA,SALSA_TOMATE_POMAROLA,320_GR,20,1.5,2.5";
			archivo3 <<n<<endl;
			n="000080,ABARROTES,EMSAL,SAL_DE_COCINA,500_GR,20,0.7,0.9";
			archivo3 <<n<<endl;
		archivo3.close();*/
}
void cargarReporteCompraTemp(string user,string bodega,string codigo,string cantidad){
	string t;
	string nombre="temporal compra cliente "+user+".txt";
	ofstream archivo(nombre.c_str(),ios::app);
	string m,n,mont="";
	float monto;
	int mo;

if(bodega=="1000"){

	string nombreArch="lista de productos bodega 1000.txt";
		string campo1,linea;
		int i;
		ifstream archiv(nombreArch.c_str());

		if(archiv.is_open()){

			while(!archiv.eof()){
				getline(archiv,linea);
				stringstream a(linea);

				while(!a.eof()){
					getline(a,campo1,',');
					if(campo1==codigo){
						string precio;
							for(i=2;i<=8;i++){
								getline(a,campo1,',');
								if(i==2){archivo<<codigo<<","<<campo1<<",";
								}
								if(i==3){archivo<<campo1<<",";
																}
								if(i==4){archivo<<campo1<<",";
								}
								if(i==5){archivo<<campo1<<",";
																										}
								if(i==8){
									precio=campo1;
									archivo<<campo1<<",";
								}

							}
							int a=0;
							for(i=0;i<precio.length();i++){
								if(precio[i]=='.'){
									a=1;
								}
							}
							if(a==1){
								monto=convertirAFloat(precio)*convertirAEntero(cantidad);
								mont=static_cast<std::ostringstream*>(&(std::ostringstream() << monto))->str();
							}else{
								mo=convertirAEntero(precio)*convertirAEntero(cantidad);
								mont=static_cast<std::ostringstream*>(&(std::ostringstream() << mo))->str();
							}
							archivo<<cantidad<<","<<mont<<","<<"fecha"<<","<<"hora"<<","<<bodega<<endl;
					}
				}
			}
		}
		archiv.close();
		archivo.close();

	}
	if(bodega=="2000"){

		string nombreArch="lista de productos bodega 2000.txt";
				string campo1,linea;
				int i;
				ifstream archiv(nombreArch.c_str());

				if(archiv.is_open()){
					while(!archiv.eof()){
						getline(archiv,linea);
						stringstream a(linea);
						while(!a.eof()){
							getline(a,campo1,',');
							if(campo1==codigo){
								string precio,mont;
									for(i=2;i<=8;i++){
										getline(a,campo1,',');
										if(i==2){archivo<<codigo<<","<<campo1<<",";
										}
										if(i==3){archivo<<campo1<<",";
																		}
										if(i==4){archivo<<campo1<<",";
										}
										if(i==5){archivo<<campo1<<",";
																												}
										if(i==8){archivo<<campo1<<",";
										precio=campo1;
										}

									}
									int a=0;
									for(i=0;i<precio.length();i++){
										if(precio[i]=='.'){
												a=1;
										}
									}
									if(a==1){
									monto=convertirAFloat(precio)*convertirAEntero(cantidad);
									mont=static_cast<std::ostringstream*>(&(std::ostringstream() << monto))->str();
									}else{
										mo=convertirAEntero(precio)*convertirAEntero(cantidad);
										mont=static_cast<std::ostringstream*>(&(std::ostringstream() << mo))->str();
									}
									archivo<<cantidad<<","<<mont<<","<<"fecha"<<","<<"hora"<<","<<bodega<<endl;
							}
						}
					}
				}
				archiv.close();
				archivo.close();
	}
	if(bodega=="3000"){

		string nombreArch="lista de productos bodega 3000.txt";
				string campo1,linea;
				int i;
				ifstream archiv(nombreArch.c_str());

				if(archiv.is_open()){
					while(!archiv.eof()){
						getline(archiv,linea);
						stringstream a(linea);
						while(!a.eof()){
							getline(a,campo1,',');
							if(campo1==codigo){
								string precio,mont;
									for(i=2;i<=8;i++){
										getline(a,campo1,',');
										if(i==2){archivo<<codigo<<","<<campo1<<",";
										}
										if(i==3){archivo<<campo1<<",";
																		}
										if(i==4){archivo<<campo1<<",";
										}
										if(i==5){archivo<<campo1<<",";
																												}
										if(i==8){
											precio=campo1;
											archivo<<campo1<<",";
										}

									}
									int a=0;
									for(i=0;i<precio.length();i++){
									    if(precio[i]=='.'){
												a=1;
										}
									}
									if(a==1){
									monto=convertirAFloat(precio)*convertirAEntero(cantidad);
									mont=static_cast<std::ostringstream*>(&(std::ostringstream() << monto))->str();
									}else{
										mo=convertirAEntero(precio)*convertirAEntero(cantidad);
										mont=static_cast<std::ostringstream*>(&(std::ostringstream() << mo))->str();
									}
									archivo<<cantidad<<","<<mont<<","<<"fecha"<<","<<"hora"<<","<<bodega<<endl;
							}
						}
					}
				}
				archiv.close();
				archivo.close();
	}

}
void cargarReporteVentaTemp(string user,string bodega,string codigo,string cantidad){
	string t;
float monto;
int mo;



		string m,n,mont="";
		if(bodega=="1000"){
			string nombre="temporal venta bodega "+bodega+".txt";
					ofstream archivo(nombre.c_str(),ios::app);

		string nombreArch="lista de productos bodega 1000.txt";
			string campo1,linea;
			int i;
			ifstream archiv(nombreArch.c_str());

			if(archiv.is_open()){
				while(!archiv.eof()){
					getline(archiv,linea);
					stringstream a(linea);
					while(!a.eof()){
						getline(a,campo1,',');
						if(campo1==codigo){
							string precio,mont;
								for(i=2;i<=8;i++){
									getline(a,campo1,',');
									if(i==2){archivo<<codigo<<","<<campo1<<",";
									}
									if(i==3){archivo<<campo1<<",";
																	}
									if(i==4){archivo<<campo1<<",";
									}
									if(i==5){archivo<<campo1<<",";
																											}
									if(i==8){
										precio=campo1;
										archivo<<campo1<<",";
									}
									}
									int a=0;
									for(i=0;i<precio.length();i++){
									if(precio[i]=='.'){
										a=1;
									}
									}
									if(a==1){
									monto=convertirAFloat(precio)*convertirAEntero(cantidad);
									mont=static_cast<std::ostringstream*>(&(std::ostringstream() << monto))->str();
									}else{
										mo=convertirAEntero(precio)*convertirAEntero(cantidad);
										mont=static_cast<std::ostringstream*>(&(std::ostringstream() << mo))->str();
									}archivo<<cantidad<<","<<mont<<","<<"fecha"<<","<<"hora"<<","<<user<<endl;
						}
					}
				}
			}
			archiv.close();
			archivo.close();

		}
		if(bodega=="2000"){
			string nombre="temporal venta bodega "+bodega+".txt";
			ofstream archivo2(nombre.c_str(),ios::app);

			string nombreArch="lista de productos bodega 2000.txt";
					string campo1,linea;
					int i;
					ifstream archiv(nombreArch.c_str());

					if(archiv.is_open()){
						while(!archiv.eof()){
							getline(archiv,linea);
							stringstream a(linea);
							while(!a.eof()){
								getline(a,campo1,',');
								if(campo1==codigo){
									string precio,mont;
										for(i=2;i<=8;i++){
											getline(a,campo1,',');
											if(i==2){archivo2<<codigo<<","<<campo1<<",";
											}
											if(i==3){archivo2<<campo1<<",";
																			}
											if(i==4){archivo2<<campo1<<",";
											}
											if(i==5){archivo2<<campo1<<",";
																													}
											if(i==8){
												precio=campo1;
												archivo2<<campo1<<",";
											}

										}
										int a=0;
										for(i=0;i<precio.length();i++){
											if(precio[i]=='.'){
												a=1;

											}
										}
										if(a==1){
											monto=convertirAFloat(precio)*convertirAEntero(cantidad);
											mont=static_cast<std::ostringstream*>(&(std::ostringstream() << monto))->str();
										}else{
											mo=convertirAEntero(precio)*convertirAEntero(cantidad);
											mont=static_cast<std::ostringstream*>(&(std::ostringstream() << mo))->str();
										}
										archivo2<<cantidad<<","<<mont<<","<<"fecha"<<","<<"hora"<<","<<user<<endl;
								}
							}
						}
					}
					archiv.close();
					archivo2.close();
		}
		if(bodega=="3000"){
			string nombre="temporal venta bodega "+bodega+".txt";
			ofstream archivo3(nombre.c_str(),ios::app);

			string nombreArch="lista de productos bodega 3000.txt";
					string campo1,linea;
					int i;
					ifstream archiv(nombreArch.c_str());

					if(archiv.is_open()){
						while(!archiv.eof()){
							getline(archiv,linea);
							stringstream a(linea);
							while(!a.eof()){
								getline(a,campo1,',');
								if(campo1==codigo){
									string precio,mont;
										for(i=2;i<=8;i++){
											getline(a,campo1,',');
											if(i==2){archivo3<<codigo<<","<<campo1<<",";
											}
											if(i==3){archivo3<<campo1<<",";
																			}
											if(i==4){archivo3<<campo1<<",";
											}
											if(i==5){archivo3<<campo1<<",";
																													}
											if(i==8){
												precio=campo1;
												archivo3<<campo1<<",";
											}
										}
											int a=0;
											for(i=0;i<precio.length();i++){
												if(precio[i]=='.'){
														a=1;
												}
											}
											if(a==1){
												monto=convertirAFloat(precio)*convertirAEntero(cantidad);
												mont=static_cast<std::ostringstream*>(&(std::ostringstream() << monto))->str();
											}else{
												mo=convertirAEntero(precio)*convertirAEntero(cantidad);
												mont=static_cast<std::ostringstream*>(&(std::ostringstream() << mo))->str();
											}
									archivo3<<cantidad<<","<<mont<<","<<"fecha"<<","<<"hora"<<","<<user<<endl;
								}
							}
						}
					}
					archiv.close();
					archivo3.close();
		}

}


float convertirAFloat(string cantidad){
	char precioEntero[0];
		char precioDecimal[0];
		string parteEntera,parteDecimal;int i,coma,tam=cantidad.length();
		for(i=0;i<tam;i++){
			if(cantidad[i]=='.'){
				coma=i;
			}
		}

		for(i=0;i<coma;i++){
			parteEntera=parteEntera+cantidad[i];
		}

		for(i=coma+1;i<tam;i++){
			parteDecimal=parteDecimal+cantidad[i];
		}

		strcpy(precioEntero,parteEntera.c_str());

		float pr;
			pr=atoi(precioEntero)*1.0;

		strcpy(precioDecimal,parteDecimal.c_str());
		pr=pr+(atoi(precioDecimal)*1.0/(pow(10,tam-1-coma)));
		return pr;
}
int convertirAEntero(string cantidad){

	char precio[0];

		strcpy(precio,cantidad.c_str());
		int pr;
		pr=atoi(precio);

			return pr;
}




void finalizarCompra(){
	mostrarReporteCompra();
	string resp;
	cout<<"Desea confirmar la compra?(S/N) ==> ";cin>>resp;
	if(resp =="S"||resp=="s"){
		modoDePago(preguntaFormaPago());
		cargarTempRealCompra();
		cargarTempRealVenta();
		//actualizarStock("1000");
		//actualizarStock("2000");
		//actualizarStock("3000");
		menuCliente();


	}else{
		menuCliente();
	}

}

void mostrarReporteCompra() {
	string nombreArchivo1="temporal compra cliente "+usuario.usuario+".txt";

		    ifstream archivo1(nombreArchivo1.c_str());
			string linea,campo1,aux;double monto=0;
			int i;
			if(archivo1.is_open()){
				cout<<" CODIGO        TIPO       MARCA       NOMBRE       UNIDAD       PRECIO       CANTIDAD      MONTO      FECHA      HORA       BODEGA "<<endl;
				while(!archivo1.eof()){
				  getline(archivo1,linea);
				  stringstream s(linea);
					while(!s.eof()){
						for(i=1;i<=11;i++){
							getline(s,campo1,',');
							cout<<campo1<<"        ";
							if(i==8){
								aux=campo1;
								monto=monto+atof(aux.c_str());

							}
								}cout<<endl;
							}

						     }


						}


			archivo1.close();
			cout<<"MONTO TOTAL = S/."<<monto<<endl;
}

void modoDePago(int modo) {
	long long numTarjeta;
	if(modo == 1) {
		cout << "Digite su numero de tarjeta "; cin >> numTarjeta;
		if(validarTarjeta(numTarjeta)) {
			cout<<"TARJETA VALIDADA....COMPRA REALIZADA CON EXITO"<<endl;
			cout<<"Se esta enviando el pedido"<<endl;

		}else{
			modoDePago(1);
		}

	} else if(modo == 2) {
		cout<<"COMPRA CONFIRMADA"<<endl;
		cout<<"Se esta enviando el pedido"<<endl;


	} else {
		cout << "Esa opcion no esta registrada";
		preguntaFormaPago();
	}
}
int preguntaFormaPago() {
	int modo;
	cout << "Uted desea pagar con: " << endl;
	cout << "[1] Tarjeta" << endl;
	cout << "[2] Efectivo" << endl;
	cout << "                  Digite la opcion!!!"; cin >> modo;
	return modo;
}
int suma_cif(int y){
	int s,d,u;
	u = y%10;
	d = y/10;
	return s=u+d;
}
bool validarTarjeta(long long n) {
	bool existe;
	long long num[17];
	int imp[9],par[9],mul[9];
	int r=0;
	for(int i=0;i<16;i++){
		num[i]=n/(pow(10,15-i));
		if(i!=15){
			n-=num[i]*pow(10,15-i);
		}
	}
	for(int i=0;i<8;i++){
		imp[i]=num[i*2];
		par[i]=num[i*2+1];
		mul[i]=par[i]*2;
		if (mul[i]>9){
			mul[i]=suma_cif(mul[i]);
		}
		r= r+imp[i]+mul[i];
	}
	if(r%10==0){
		existe=true;
	}else{
		existe=false;
	}
	return existe;
}
void cargarTempRealCompra(){
	string nombre="compra cliente "+usuario.usuario+".txt";
				ofstream archivo3(nombre.c_str(),ios::app);

				string nombreArch="temporal compra cliente "+usuario.usuario+".txt";;
						string campo1,linea;

						ifstream archiv(nombreArch.c_str());

						if(archiv.is_open()){
							while(!archiv.eof()){
								getline(archiv,linea);
								archivo3<<linea<<endl;
							}
						}
					archivo3.close();
			archiv.close();
    borrarTemp(nombreArch);


}
void cargarTempRealVenta() {
	string nombre="venta bodega 1000.txt";
					ofstream archivo1(nombre.c_str(),ios::app);

					string nombreArch="temporal venta bodega 1000.txt";;
							string campo1,linea;

							ifstream archiv(nombreArch.c_str());

							if(archiv.is_open()){
								while(!archiv.eof()){
									getline(archiv,linea);
									archivo1<<linea<<endl;
								}
							}
						archivo1.close();
				archiv.close();
				borrarTemp(nombreArch);

				string nombre2="venta bodega 2000.txt";
									ofstream archivo2(nombre2.c_str(),ios::app);

									string nombre2Arch="temporal venta bodega 2000.txt";;


											ifstream archiv2(nombre2Arch.c_str());

											if(archiv2.is_open()){
												while(!archiv2.eof()){
													getline(archiv2,linea);
													archivo2<<linea<<endl;
												}
											}
										archivo2.close();
								archiv2.close();
								borrarTemp(nombre2Arch);
								string nombre3="venta bodega 3000.txt";
													ofstream archivo3(nombre3.c_str(),ios::app);

													string nombre3Arch="temporal venta bodega 3000.txt";;


															ifstream archiv3(nombre3Arch.c_str());

															if(archiv3.is_open()){
																while(!archiv3.eof()){
																	getline(archiv3,linea);
																	archivo3<<linea<<endl;
																}
															}
														archivo3.close();
												archiv3.close();
												borrarTemp(nombre3Arch);


}
void borrarTemp(string nombre) {

	remove(nombre.c_str());
}


vector<string> leerLineaArchivoTempComp(string linea, char separador){
	vector<string> valores;
	string aux="";
	for(int i=0;i<linea.size();i++){
		if(linea[i]==separador){
			valores.push_back(aux);
			aux="";
		}else{
			aux+=linea[i];
		}
	}
	valores.push_back(aux);
	return valores;
}

struct registroCompra leerDatosRegistroCompra(string linea, char separador){
	vector<string> datos = leerLineaArchivoTempComp(linea,separador);
	struct registroCompra respuesta;
	respuesta.inicializar(datos);
	return respuesta;
}


//###########      Reporte de Usuarios        ###################
void buscarFiltro(int filtroI,string filtroS, string nombreArchivo){
			ifstream archivo1(nombreArchivo.c_str());
			string linea,campo1;
			int i=1;
			if(archivo1.is_open()){
				while(!archivo1.eof()){
					getline(archivo1,linea);
					stringstream s(linea);
					while(!s.eof() ){
						getline(s,campo1,',');
						if(i==filtroI){
							if(campo1==filtroS){
								cout<<linea<<endl;
							}
						}
							i++;
					}
					i=1;
				}
				archivo1.close();
			}else{
				cout<<"No se pudo abrir el archivo"<<endl;
			}
			menuAdministrador();
}

void reporteDeUsuarios(){


	int filtroI,opcion1;
	string nombreArchivo,filtroS;

	cout<<"OPCIONES"<<endl;
	cout<<"(1)Reporte de Clientes"<<endl;
	cout<<"(2)Reporte de Bodegueros"<<endl;
	cout<<"(3)Regresar"<<endl;
	cin>>opcion1;
	if(opcion1==1){
		cout<<"Buscar por:"<<endl;
		cout<<"(1)Codigo de Usuario"<<endl;
		cout<<"(2)Nombre"<<endl;
		cout<<"(3)Apellido"<<endl;
		cout<<"(4)Sexo"<<endl;
		cout<<"(5)Correo"<<endl;
		cout<<"(6)Direccion"<<endl;
		cout<<"(7)Telefono"<<endl;
		cin>>filtroI;

		cout<<"Digite lo que desea buscar"<<endl;
		cin>>filtroS;

		nombreArchivo="registro de cliente.txt";

		buscarFiltro(filtroI,filtroS,nombreArchivo);


	}else if(opcion1==2){
		cout<<"Buscar por: "<<endl;
		cout<<"(1)Codigo de Usuario"<<endl;
		cout<<"(2)Nombre"<<endl;
		cout<<"(3)Apellido"<<endl;
		cout<<"(4)Bodega"<<endl;
		cout<<"(5)Ruc"<<endl;
		cout<<"(6)Sexo"<<endl;
		cout<<"(7)Correo"<<endl;
		cout<<"(8)Direccion"<<endl;
		cout<<"(9)Telefono"<<endl;
		cin>>filtroI;

		cout<<"Digite lo que desea buscar"<<endl;
		cin>>filtroS;

		nombreArchivo="registro de bodegueros.txt";

		buscarFiltro(filtroI,filtroS,nombreArchivo);

	}else if(opcion1==3){
		system("cls");
		menuAdministrador();
	}else if(opcion1!=1 or opcion1!=2 or opcion1!=3){
		cout<<"Opcion ingresada no valida"<<endl;
		reporteDeUsuarios();
	}





}
//##############################


//##########       Reporte de Clientes     ####################
void reportarCompra(){
	int filtroI;
	string nombreArchivo,filtroS,codCliente;
	cout<<"Ingrese el codigo del cliente : ";cin>>codCliente;cout<<endl;


	cout<<"Buscar por: "<<endl;
	cout<<"(1)Codigo"<<endl;
	cout<<"(2)Tipo"<<endl;
	cout<<"(3)Nombre"<<endl;
	cout<<"(4)Fecha"<<endl;//fecha es el campo numero 8
	cout<<"(5)Hora"<<endl;//hora es el campo numero 9
	cout<<"(6)Regresar"<<endl;

	cin>>filtroI;
	if(filtroI==4){
		filtroI=8;
	}else if(filtroI==5){
		filtroI=9;
	}else if(filtroI==6){
		menuAdministrador();
	}
	if(filtroI>9){
		cout<<"Opcion ingresada no valida"<<endl;
		reportarCompra();
	}

	cout<<"Digite lo que desea buscar"<<endl;
	cin>>filtroS;

	nombreArchivo="compra cliente "+codCliente+".txt";

	buscarFiltro(filtroI,filtroS,nombreArchivo);
}

void reporteClientes(){
	reportarCompra();
}
//##############################


//############     Reporte de Bodegueros    ###################
void reportarVenta(){
		int filtroI;
		string nombreArchivo,filtroS,codbo;

		cout<<"Ingrese el codigo de la bodega : ";cin>>codbo;cout<<endl;

		cout<<"Buscar por: "<<endl;
		cout<<"(1)Codigo"<<endl;
		cout<<"(2)Tipo"<<endl;
		cout<<"(3)Nombre"<<endl;
		cout<<"(8)Fecha"<<endl;
		cout<<"(9)Hora"<<endl;

		cin>>filtroI;

		cout<<"Digite lo que desea buscar"<<endl;
		cin>>filtroS;

		nombreArchivo="venta bodega "+codbo+".txt";

		buscarFiltro(filtroI,filtroS,nombreArchivo);
}
void reportarOperaciones(){
	int filtroI;
	string nombreArchivo,filtroS;

	cout<<"Buscar por: "<<endl;
	cout<<"(1)Tipo"<<endl;
	cout<<"(2)Codigo"<<endl;
	cout<<"(3)Fecha"<<endl;
	cout<<"(4)Hora"<<endl;

	cin>>filtroI;

	cout<<"Digite lo que desea buscar"<<endl;
	cin>>filtroS;

	nombreArchivo="reporteOperaciones.txt";

	buscarFiltro(filtroI,filtroS,nombreArchivo);
}
void reporteBodeguero(){
	int opcion;
	cout<<"OPCIONES"<<endl;
	cout<<"(1)Reporte de ventas"<<endl;
	cout<<"(2)Reporte de Operaciones"<<endl;
	cout<<"(3)Regresar"<<endl;
	cin>>opcion;

	if(opcion==1){
		reportarVenta();
	}else if(opcion==2){
		reportarOperaciones();
	}else if(opcion==3){
		menuAdministrador();
	}
}
//##############################








//##########     Menu de Administrador     ####################
void menuAdministrador(){
	cout<<"OPCIONES"<<endl;
	cout<<"(1) Reporte de Usuarios"<<endl;
	cout<<"(2) Reporte de Clientes"<<endl;
	cout<<"(3) Reporte de Bodeguero"<<endl;
	cout<<"(4) Salir Sesion"<<endl;
	int option;
	cout<<"Elija una opcion ==> ";cin>>option;cout<<endl;
	switch(option){
		case 1:
			reporteDeUsuarios();
			break;
		case 2:
			reporteClientes();break;
		case 3:
			reporteBodeguero();break;
		case 4:
			MenuDeEntrada();break;
		default:
			cout<<"Opcion no valida"<<endl;
			MenuDeEntrada();

		}
}
//##############################

//Registrar Nuevo Producto
bool validarCodigo(string cod, string nombreArchivo){
	bool n=false;


		    ifstream archivo1(nombreArchivo.c_str());
			string linea,campo1;
			int i;
			if(archivo1.is_open()){
				while(!archivo1.eof()){
				  getline(archivo1,linea);
				  stringstream s(linea);
					while(!s.eof()){

							for(i=1;i<=5;i++){
								getline(s,campo1,',');
								if(i==2){
									if(campo1==cod){
										n=true;
									}


									}
							}

						     }


						}

				}
						archivo1.close();
						if(n==true){
							cout<<"CODIGO YA EXISTE"<<endl;
							menuBodeguero();
						}
						return n;
}
void cargarDatosNuevoProducto(producto p1){
    ofstream archivo;
	archivo.open("lista general de productos.txt",ios::app);
    archivo<<p1.tipo<<","<<p1.codigo<<","<<p1.marca<<","<<p1.nombre<<","<<p1.unidad<<endl;
    archivo.close();
    string arch="lista de productos bodega "+bode.usuario+".txt";
    ofstream archivo1(arch.c_str(),ios::app);
    archivo1<<p1.codigo<<","<<p1.tipo<<","<<p1.marca<<","<<p1.nombre<<","<<p1.unidad<<","<<p1.stock<<","<<p1.pcompra<<","<<p1.pventa<<endl;
    archivo1.close();
    string ar="registro de productos bodega "+bode.usuario+".txt";
    ofstream a(ar.c_str(),ios::app);
    a<<p1.codigo<<","<<p1.tipo<<","<<p1.nombre<<","<<"fecha"<<","<<"hora"<<endl;
    a.close();
}


//Consultar Registro
void mostrarRegistro(string f){
	string parch="registro de productos bodega "+bode.usuario+".txt";
	ifstream arch(parch.c_str());
	string linea,campo1;
			int i;
			if(arch.is_open()){
				while(!arch.eof()){
				  getline(arch,linea);
				  stringstream s(linea);
					while(!s.eof()){
						for(i=1;i<=5;i++){
							getline(s,campo1,',');
					     	if(campo1==f){
					  		cout<<linea<<endl;

						  }
						}

						     }


						}

		}
		arch.close();
  menuBodeguero();

}

//Consultar Venta
void mostrarVenta(string f){
	string parch="venta bodega "+bode.usuario+".txt";
		ifstream arch(parch.c_str());
		string linea,campo1;
				int i;
				if(arch.is_open()){
					while(!arch.eof()){
					  getline(arch,linea);
					  stringstream s(linea);
						while(!s.eof()){
							for(i=1;i<=11;i++){
								getline(s,campo1,',');
						     	if(campo1==f){
						  		cout<<linea<<endl;

							  }
							}

							     }


							}

			}
			arch.close();
	  menuBodeguero();
}

//Consultar Producto
void mostrarProductos(string f){
	string parch="lista de productos bodega "+bode.usuario+".txt";
			ifstream arch(parch.c_str());
			string linea,campo1;
					int i;
					if(arch.is_open()){
						while(!arch.eof()){
						  getline(arch,linea);
						  stringstream s(linea);
							while(!s.eof()){
								for(i=1;i<=8;i++){
									getline(s,campo1,',');
							     	if(campo1==f){
							  		cout<<linea<<endl;

								  }
								}

								     }


								}

				}
				arch.close();
		  menuBodeguero();
}

//Menu del Bodeguero
void RegistrarNuevoProducto(){
	string b;
    int a;
	producto x;
	string Inventario1;

	cout<<" (1) Registrar nuevo producto "<<endl;
	cout<<" (2) Volver "<<endl;
	cout<<" Ingrese opcion: ";cin>>a;cout<<endl;
	string nombreArchivo2 = "tipos de producto.txt";
						ifstream archio(nombreArchivo2.c_str());

	switch(a){
		case 1:
			cout<<"Ingrese el codigo del nuevo producto: ";cin>>x.codigo;
			if(!validarCodigo(x.codigo,"lista general de productos.txt")){

					if(archio.is_open()){
						string linea;
						int i=1;
						while(!archio.eof()){
						getline(archio,linea);
						cout<<"("<<i<<")"<<linea<<endl;
						i++;
						}
					archio.close();
					int w;
				cout<<"Ingrese el tipo de producto ==> ";cin>>w;cout<<endl;
				switch(w){
				case 1 : x.tipo="PERFUMERIA";break;
				case 2 : x.tipo="LIMPIEZA";break;
				case 3 : x.tipo="BEBIDAS-CERVEZAS";break;
				case 4 : x.tipo="DESAYUNOS-DULCES";break;
				case 5 : x.tipo="QUESOS-FIAMBRES";break;
				case 6 : x.tipo="LACTEOS-HUEVOS";break;
				case 7 : x.tipo="FRUTAS-VERDURAS";break;
				case 8 : x.tipo="ABARROTES";break;
				default: menuBodeguero();
				}
				cout<<"Marca: ";cin>>x.marca;cout<<endl;
				cout<<"Nombre: ";cin>>x.nombre;cout<<endl;
				cout<<"Unidad: ";cin>>x.unidad;cout<<endl;
				cout<<"Stock: ";cin>>x.stock;cout<<endl;
				cout<<"Precio de Compra: ";cin>>x.pcompra;cout<<endl;
				cout<<"Precio de Venta: ";cin>>x.pventa;cout<<endl;
				cargarDatosNuevoProducto(x);
				cout<<"PRODUCTO REGISTRADO CON EXITO"<<endl;
				menuBodeguero();
			}

				menuBodeguero();
			;break;

	   case 2:
		menuBodeguero();break;
   	default: menuBodeguero();
	}
}
}
void consultarRegistro(){
	string filtro;
	int a;
    cout<<"Consulta de registro de nuevos productos"<<endl;
	cout<<"1. Codigo"<<endl;
	cout<<"2. Fecha"<<endl;
	cout<<"3. Hora"<<endl;
	cout<<"4. Volver"<<endl;
    cout<<"Ingrese filtro de búsqueda: ";
    cin>>a;cout<<endl;
    switch(a){
        case 1:cout<<"Ingrese Codigo: ";cin>>filtro;
             mostrarRegistro(filtro);break;
        case 2:cout<<"Ingrese Fecha de realizacion: ";cin>>filtro;
             mostrarRegistro(filtro);break;
        case 3:cout<<"Ingrese Hora de realizacion: ";cin>>filtro;
             mostrarRegistro(filtro);break;
        default: menuBodeguero();
    }

}

void consultarVenta(){
	string filtro;
	int a;
	cout<<"***Consulta de ventas***"<<endl;
	cout<<"1. Codigo"<<endl;
	cout<<"2. Fecha"<<endl;
	cout<<"3. Hora"<<endl;
	cout<<"4. Tipo"<<endl;
	cout<<"5. Cliente"<<endl;
	cout<<"6. Volver"<<endl;
    cout<<"Ingrese filtro de búsqueda: ";
    cin>>a;cout<<endl;
    switch(a){
    case 1:;cout<<"Ingrese Codigo: ";cin>>filtro;
    	mostrarVenta(filtro);break;
    case 2:;cout<<"Ingrese Fecha: ";cin>>filtro;
		mostrarVenta(filtro);break;
    case 3:;cout<<"Ingrese Hora: ";cin>>filtro;
		mostrarVenta(filtro);break;
    case 4:;cout<<"Ingrese Tipo: ";cin>>filtro;
		mostrarVenta(filtro);break;
    case 5:;cout<<"Ingrese Cliente: ";cin>>filtro;
		mostrarVenta(filtro);break;
    default: ;menuBodeguero();
    }
}

void consultarProductos(){
	string filtro;
		int a;
	    cout<<"***Consulta de productos***"<<endl;
	    cout<<"1. Codigo"<<endl;
		cout<<"2. Tipo"<<endl;
		cout<<"3. Nombre"<<endl;
	    cout<<"Ingrese filtro de búsqueda: ";
	    cin>>a;cout<<endl;
	    switch(a){
	       case 1:cout<<"Ingrese Codigo: ";cin>>filtro;
	            mostrarProductos(filtro);break;
	       case 2:cout<<"Ingrese Tipo de producto: ";cin>>filtro;
	            mostrarProductos(filtro);break;
	       case 3:cout<<"Ingrese Nombre de producto: ";cin>>filtro;
	            mostrarProductos(filtro);break;
	       default: menuBodeguero();
	    }
}
/*
void actualizarStock(string bodega){
    int i,w=0;
     pro pro[1000];
    codigo producto[1000];
	string t,linea,campo;
	string nombreArch3="venta bodega "+bodega+".txt";
			        string campo3;
					ifstream archivo4(nombreArch3.c_str());


					if(archivo4.is_open()){
						while(!archivo4.eof()){

							getline(archivo4,linea);
							stringstream a(linea);
							while(!a.eof()){
								for(i=1;i<=11;i++){
									getline(a,campo3,',');
									if(i==1){
										w++;
										producto[w].producto=campo3;
									}
									if(i==7){

										producto[w].cantidad=atoi(campo3.c_str());



									}
								}


							}
						}
					}
   archivo4.close();
   int q,j;
   bool encontrado=false;
   string nom="lista de productos bodega "+bodega+".txt";
   ifstream arch(nom.c_str());
   if(arch.is_open()){
   						while(!arch.eof()){

   							getline(arch,linea);
   							stringstream a(linea);
   							while(!a.eof()){
   								for(i=1;i<=8;i++){
   									getline(a,campo3,',');


   									if(i==1){
   										for(j=1;j<=w;j++){
   											if(campo3==producto[j].producto){
   												q=j;
   												pro[q].codigo=campo3;
   												encontrado=true;
   											}
   										}


   									}
   									if(encontrado && i==2){
   										pro[q].tipo=campo3;
   									}
   									if(encontrado && i==3){
   									   										pro[q].marca=campo3;
   									   									}
   									if(encontrado && i==4){
   									   										pro[q].nombre=campo3;
   									   									}
   									if(encontrado && i==5){
   									   										pro[q].unidad=campo3;
   									   									}
   									if(encontrado && i==6){
   										producto[q].stock=atoi(campo3.c_str());

   									}
   									if(encontrado && i==7){
   									   										pro[q].pcompra=campo3;
   									   									}
   									if(encontrado && i==8){
   									   										pro[q].pventa=campo3;
   									   									}

   								}


   							}
   						}
   					}
      arch.close();
      for(i=1;i<=w;i++){
    	producto[i].stock=producto[i].stock-producto[i].cantidad;
    	pro[i].stock=static_cast<std::ostringstream*>(&(std::ostringstream() << producto[i].stock))->str();
      }


      string n="aux lista.txt";
      string te="lista de productos bodega "+bodega+".txt";
      ifstream archi(t.c_str());
      ofstream ar(n.c_str(),ios::app);
      if(archi.is_open()){
      						while(!archi.eof()){

      							getline(archi,linea);
      							stringstream a(linea);
      							while(!a.eof()){
      								for(i=1;i<=11;i++){
      									getline(a,campo3,',');
      									if(i==1){
      										for(j=1;j<=w;j++){
      										   if(campo3==producto[j].producto){
      										   		encontrado=true;
      											   ar<<pro[j].codigo<<","<<pro[j].tipo<<","<<pro[j].marca<<","<<pro[j].nombre<<","<<pro[j].unidad<<","<<pro[j].stock<<","<<pro[j].pcompra<<","<<pro[j].pventa;
      										   }else{encontrado=false;}
      								      	}

                                        }
      									if(!encontrado){
      										ar<<campo3<<",";
      									}
      								}
      							}ar<<endl;
      						}
      }
      ar.close();
      archi.close();
      remove(te.c_str());
      rename(n.c_str(),te.c_str());


}
*/







